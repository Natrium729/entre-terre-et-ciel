"Entre Terre et Ciel" by Natrium729

[
Pour faire bref :
Ce code source est libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.

Pour faire long, consultez : http://www.gnu.org/licenses
]

Volume 1 - Initialisation

Book 1 - Extensions

Part 1 - French

Include French by Eric Forgeot. Use French 1PSPr Language.

Part 2 - Basic Screen Effects

Include Basic Screen Effects by Emily Short.


Book 2 - Options

Use full-length room descriptions.
The maximum score is 2.

Book 3 - Nouveautés

Part 1 - Compréhension

Understand "b" as down.
Understand "h" as up.

Part 2 - Prier

Praying is an action applying to nothing.
Understand "prier" as praying.

Report praying:
	say "Je m'incline et commence à murmurer des paroles sacrées. Je me relève enfin après quelques secondes. Une prière ne fait jamais de mal.".


Volume 2 - Jeu

Book 1 - Introduction

Part 1 - État initial

Chapter 1 - Héros

In the grand-place is a man called the Nâga. The printed name of the Nâga is "moi-même".
The Nâga can be bloody. The Nâga is not bloody.
The description of the Nâga is "Je suis un jeune Nâga ordinaire, qui vient de finir mes neuf mues, et mes écailles bleues indiquent maintenant que je suis un adulte. Choisi par la prophétie, je suis censé vaincre le Démon de la Terre. Est-ce possible pour un être normal tel que moi ?[if bloody][line break]Je suis couvert de sang[end if].". The player is the Nâga.

In the hall is a man called the spirit. The printed name of the spirit is "moi-même".
The description of the spirit is "Je suis un jeune Nâga ordinaire, qui vient de finir mes neuf mues, et mes écailles bleues indiquent maintenant que je suis un adulte. Choisi par la prophétie, je suis censé vaincre le Démon de la Terre. Est-ce possible pour un être normal tel que moi ?[line break]Je suis étrangement transparent.".
The spirit can be foggy. The spirit is not foggy.

Chapter 2 - Orbe

Section 1 - Orbe sur Terre

The orbe is a proper-named thing carried by the Nâga.
The printed name is "l'Orbe Étincelante".
The description is "L'Orbe Étincelante, seule arme capable de vaincre le Démon de la Terre. Elle brille de mille éclats et dégage une douce chaleur.".
The orbe can be activated. It is not activated

Instead of dropping the orbe, say "Pas question d'abandonner !".

Report examining the activated orbe:
	display the boxed quotation "Écoute-moi.".

Instead of listening to the activated orbe for the first time:
	say "J'approche l'Orbe de mon oreille. Tout d'abord, rien ne se fait entendre ; puis, petit à petit, une douce mélopée s[']élève. Envoûté, je ferme les yeux.";
	wait for any key;
	say "Quand je les rouvre enfin, je découvre un lieu inconnu, nimbé de lumière. Où suis-je ?";
	wait for any key;
	display the boxed quotation "Trouve-moi.";
	now the player is the spirit;
	try looking.

Instead of listening to the activated orbe:
	say "De nouveau, la douce mélopée se fait entendre. De nouveau, je ferme les yeux. Et quand je les rouvre…[line break]";
	wait for any key;
	now the player is the spirit;
	try looking.

Instead of listening to the activated orbe when the player is bloody, say "J'approche l'Orbe de mon oreille. Pourtant, la mélodie ne vient pas. Où est donc le problème ?".

Before listening to the activated orbe when the player carries the crâne, now the spirit is in templenei

Section 2 - Orbe dans le ciel

The orbe2 is a proper-named thing carried by the spirit.
The printed name is "l'Orbe Étincelante".
Understand "orbe" as the orbe2.
The description is "L'Orbe Étincelante, seule arme capable de vaincre le Démon de la Terre. Elle brille de mille éclats et dégage une douce chaleur.".

Instead of dropping the orbe2, say "Pas question d'abandonner !".

Instead of listening to the orbe2:
	say "De nouveau, la douce mélopée se fait entendre. De nouveau, je ferme les yeux. Et quand je les rouvre…[line break]";
	wait for any key;
	now the player is the Nâga;
	try looking.


Part 2 - Intro

When play begins:
	say "Jamais je n'avais nagé aussi vite. J'avais mal et j'aurais aimé m'arrêter, mais je ne pouvais pas.";
	wait for any key;
	say "L'Orbe. Il n'y avait que cela qui comptait désormais. L'avenir du monde reposait littéralement entre mes mains.";
	wait for any key;
	say "L'Orbe, seule arme capable de vaincre le Démon de la Terre, éveillé après un long sommeil de cent mille ans.";
	wait for any key;
	say "Et moi, jeune Nâga que la prophétie annonçait comme le sauveur, devais le vaincre. Mais comment ?";
	wait for any key;
	say "[line break]J'aperçus ma cité en contrebas : je le saurais bientôt…[paragraph break]";
	wait for any key.

Book 2 - Terre

Terre is a region.
Index map with room-colour of terre set to "Dark Golden Rod".

Part 1 - Grand-place

The grand-place is a room in terre. "La place pavée sur laquelle nous nous sommes réunis lorsque la terre avait tremblé. Elle est maintenant vide, et ma présence ne semble pas avoir été remarquée. Heureusement, je pense, car je n'aurais pas aimé me voir acclamé par la foule alors que le monde n'est pas encore hors de danger[first time]. Je devrais aller à la rencontre de la prêtresse[only][if the autel is activated]. [line break][one of]Il semble y avoir eu une explosion et les dalles ont volé en éclats : une entrée s'est creusée dans le fond de l'océan[or]Un tunnel béant s'ouvre au centre de la place[stopping][end if].[line break]Le temple se dresse au sud.".
The printed name is "Sur la grand-place".

Instead of going nowhere from the grand-place, say "Il me reste encore beaucoup à faire avant de pouvoir me promener de nouveau dans ma cité bien-aimée.".

Instead of going from the grand-place to the tunnel when the autel is not activated, say "Je ne peux pas aller dans cette direction.".

After going to the grand-place when the autel is activated:
	say "Alors que je retourne sur la grand-place, je vois que le sol a volé en éclat, révélant un tunnel qui s'enfonce au plus profond de la Terre.";
	continue the action

Chapter 1 - Temple (objet)

The templenaga is a scenery in the grand-place.
The printed name is "temple".
Understand "temple" as the templenaga.
The description is "Notre temple, où nous vénérons notre Déesse. Maintenant, il me rappelle mes aventures sur cette île où habitaient nos ancêtres.".

Instead of entering the temple, try going south.


Part 2 - Temple

The temple is south from the grand-place. The temple is inside from the grand-place. It is in terre. "Notre lieu de culte, drapé de pénombre et silencieux, comme toujours. Maintenant que je le sais, je trouve étrange que nous vénérions la Déesse du Ciel, alors qu'on ne le voit jamais depuis le fond de l'océan. La prêtresse le sait-elle ?".
The printed name is "Dans le temple sacré de notre Déesse".

Instead of exiting when the location is temple, try going north.

Index map with the temple mapped east from the hall.

Chapter 1 - Prêtresse

The prêtresse is a woman in the temple. "La prêtresse semble [if the orbe is not activated]m'attendre[else]froissée[end if]."
The indefinite article is "la".
Understand "pretresse" as the prêtresse.
The description is "Prêtresse est une haute fonction chez notre peuple. Elle dirige notre culte et nous empêche de nous éloigner du droit chemin. Notre prêtresse actuelle est avisée et nous guide avec sagesse. Elle saura que faire.".

Instead of giving the orbe to the prêtresse, try talking to the prêtresse.

Instead of talking to the prêtresse for the first time:
	say "« Je t'attendais, déclare-t-elle en me voyant entrer. As-tu réussi ?";
	wait for any key;
	say "[line break]— Oui, répondissé-je en lui tendant l'Orbe. Voici ce que j'ai trouvé sur l[']île. Mais que dois-je faire maintenant ? »";
	wait for any key;
	say "[paragraph break]Elle ne dit rien et se saisit de l'objet. Elle l'examine attentivement, semble réfléchir intensément, et se décide finalement à le poser sur l'autel. Elle murmure alors d'inaudibles prières…";
	wait for any key;
	say " mais rien ne se passe ; elle me prie de le reprendre, l'air dépitée.";
	wait for any key;
	say "[line break]« Je ne sais pas, finit-elle par annoncer. Je pense que c'est au sauveur de la prophétie de découvrir le secret de cet artefact. Je ne peux que te souhaiter bonne chance. Je suis désolée. »";
	wait for any key;
	say "[paragraph break]Je connais notre prêtresse. Après un tel échec, elle ne parlera plus. Que puis-je faire, dans ce cas ?";
	wait for any key;
	now the orbe is activated.
	
Instead of talking to the prêtresse, say "Après cet échec, elle ne parlera plus.".

Part 3 - Tunnel

Instead of going from the tunnel to the grand-place, say "Je ne peux pas revenir en arrière, malgré mon appréhension de continuer".

Before going to the tunnel when the autel is activated for the first time:
	say "Hésitant, je m'avance dans le trou nouvellement apparu. Je nage dans un boyau qui paraît sans fin et qui s'enfonce au plus profond de la terre. Enfin, j'en atteins le bout, une poche d'air. J[']émerge.";
	wait for any key.

The tunnel is down from the grand-place. It is in terre. "Un sombre couloir au cœur de la terre. À l'une des extrémités, de l'eau, directement reliée jusqu'au fond de l'océan. De l'autre, à l'est, une sinistre lueur rougeoyante m'attend.".
The printed name is "Le long d'un tunnel".

Chapter 1 - Eau

The eau is a scenery in tunnel.
the description is "L'eau de l'océan, qui m'a conduit jusqu'ici.".

Understand "laver [something]" as rubbing.

Instead of rubbing the player in the tunnel when the player is bloody:
	say "Je plonge dans l'eau et me lave de tout ce sang démoniaque. Enfin, je ressors.";
	now the player is not bloody.

Chapter 2 - Lueur

The lueur is a scenery in tunnel.
The description is "Une inquiétante lueur rouge. Elle provient de l'est.".

Part 4 - Entrée

The entrée is east from the tunnel. It is in terre. "[if the lave is solid]La lave s'est solidifiée comme par magie, mais la chaleur reste présente. Je devrais continuer au nord[else]À peine fait-on quelques pas que la chaleur se fait ressentir ; en effet, cet endroit n'est qu'un mince bras de roche relié à une île minuscule entourée de lave en fusion. Impossible de traverser ce magma brûlant sans pont. Pourtant, je vois une entrée au nord[end if].".
The printed name is "Sur une île entourée de lave [if the lave is solid]solidifiée[else]en fusion[end if]".

After going to the entrée:
	display the boxed quotation
	"L'Orbe te protège de la chaleur.";
	continue the action.

Instead of going north from the entrée when the lave is not solid:
	say "Une petite voix en moi me dit que je n'ai rien à craindre, et j'avance avec détermination vers la lave, la croyant.";
	wait for any key;
	say "Pourtant, elle se trompait.";
	wait for any key;
	end the story saying "Vous êtes mort !".

Chapter 1 - Œil

The oeil is a fixed in place thing in the entrée. "[if bursted]Une horreur sanglante[else]Un énorme œil[end if] est enchâssé dans la roche au centre de l[']île.".
The printed name is "œil".
The description is "Un [if bursted]reste d[']œil, éclaté et sanglant[else]énorme globe oculaire injecté de sang, ne restant jamais immobile plus d'une seconde. Une telle chose ne peut être que démoniaque[end if].".
The oeil can be bursted. The oeil is not bursted.

Understand "crever [something]" as attacking.

Instead of attacking the oeil:
	if the player carries the stalagmite
	begin;
		say "Je lève la stalagmite haut par-dessus ma tête, et l'abat sur l'immonde œil. Il éclate, et le sang gicle sur moi. Je crois distinguer un lointain et grave cri de douleur.[line break]Ne voyant pas l'intérêt de garder la stalagmite, je m'en débarrasse.";
		now the player is bloody;
		now the oeil is bursted;
		remove the stalagmite from play;
		display the boxed quotation "Reviens.";
	else;
		say "Pas avec mes mains nues.";
	end if.

Chapter 2 - Stalagmite

The stalagmite is female thing in the entrée. "Une longue stalagmite esseulée tente d'atteindre le plafond, en vain.".
The description is "[if handled]La salagmite que j'ai arrachée à la terre. Elle fait une bonne arme malgré son poids énorme[else]Une longue colonne pointue, dressée vers les noires hauteurs, mais ne les atteignant pas[end if].".

Instead of taking the stalagmite for the first time, say "Rassemblant mes forces, je saisis la stalagmite et tente de la briser. Je relâche bientôt l'effort. Il faudra que je réessaie.".

Instead of attacking the not handled stalagmite, try taking the stalagmite.

Chapter 3 - Lave

The lave is scenery in the entrée.
The description is "[if solid]La lave s'est solidifiée comme par magie, et me permet maintenant de continuer vers le nord[else]La lave en fusion rend toute progression impossible[end if].".
The lave can	be solid. It is not solid.


Part 5 - Campement

The campement is north from the entrée. It is in terre. "[first time]Je m'enfonce de plus en plus profondément dans la Terre. La chaleur augmente et la luminosité baisse. [only]Me voici dans un étrange campement. Des pieux entourent cet endroit dans un semblant de barricade et des débris témoignent que quelque chose a déjà vécu ici. Mais quoi ? Seraient-ils encore là ? Et surtout, représenteraient-ils un danger ?[line break]Il semblerait que le chemin continue au nord.".
The printed name is "Au cœur d'un campement abandonné".

Chapter 1 - Débris

Some débris are scenery in the campement.
Understand "debris" as the débris.
The description is "Les gens qui vivaient ici semblent avoir quitté précipitamment les lieux. Étrangement, tout est resté intact ; seule la poussière témoigne du temps qui a passé. Qu'est-ce qui a bien pu se passer ici ? Cela fait froid dans le dos.".

Chapter 2 - Coffre

The coffre is an openable closed locked container in the campement.
The description is "Ce petit coffre paraissant millénaire m'intrigue. Il a l'air de contenir quelque chose de précieux, pourtant il a été abandonné. Que peut-il bien renfermer ?".

Chapter 3 - Note

The note is a thing in the coffre.
The description is "Un petit morceau de papier jauni, ayant résisté au temps. Malheureusement, son contenu est écrit dans une langue qui m'est inconnue. Cependant, quelque chose a été griffonné sur le verso : un réseau compliqué de lignes et colonnes s'entrelaçant, rappelant le plan d'une ville, ainsi qu'une flamme.".

Instead of reading the note, say "Je n'arrive pas à comprendre ce langage.".

Carry out examining the note for the first time:
	increment the score.

Chapter 4 - Lampe

The lampe is a female thing in the campement.
The description is "Une antique lampe, semblant avoir résisté au temps. Elle contient un dispositif optique compliqué, témoin d'une science avancée. [if the cristal is part of the lampe]Le cristal est inséré dans la lampe, qui est allumée[else]Elle pourrait m[']être utile, si je savais l'allumer[end if]."

Instead of switching on the lampe when the cristal is not part of the lampe, say "Il n'y a aucun dispositif permettant de l'allumer. Cependant, je peux voir une sorte de socle à l'intérieur.".

Instead of switching on the lampe when the cristal is part of the lampe, say "Elle est déjà allumée.".

Instead of switching off the lampe when the cristal is part of the lampe, say "Je dois retirer le cristal pour l[']éteindre.".

Chapter 5 - Sac

The sac is an openable closed not lockable container in the campement.
The description is "Une vieille besace tombant presque en morceaux. De même, elle a été abandonnée. Pourquoi donc ?".
Understand "besace" as the sac.

Chapter 6 - Cristal

The cristal is a thing in the sac.
The description of the cristal is "Une pierre faiblement luminescente, que ne doit se former qu'ici, dans les profondeurs de la terre[if the cristal is part of the lampe]. La lampe amplifie son rayonnement, et elle éclaire désormais beaucoup plus loin[end if].".

Instead of inserting the cristal into the lampe:
	say "J'insère la pierre dans la lampe ; aussitôt, un jeu de miroirs et de lentilles amplifie la lumière du cristal. La lampe est maintenant allumée.";
	now the lampe is lit;
	now the cristal is part of the lampe.

Instead of taking or taking off the cristal when the cristal is part of the lampe:
	say "Je retire le cristal de la lampe. Un petit clic se fait entendre, et la lumière disparaît. La lampe est maintenant éteinte.";
	now the lampe is unlit;
	now the player carries the cristal.


Part 6 - Carrière

The carrière is north from the campement. It is dark. It is in terre. "Cette cavité ne semble pas naturelle. Elle paraît avoir été creusée à la pioche, sûrement pour récupérer ces cristaux luminescents qui parsèment les parois. L'une d'elles est percée d'un mince boyau à l'ouest, et un passage au nord [if the gravats are closed]a été bouché par des gravats[else]est maintenant ouvert[end if].".
The printed name is "Dans une carrière désaffectée".

Rule for printing the description of a dark room when the location is carrière:
	say "L'obscurité est constellée de centaines petites lumières, malheureusement pas assez fortes pour y voir clair." instead.

Rule for printing the announcement of darkness when the location is carrière:
	say "L'obscurité revient, et avec elle les centaines de pâles lumières.".


Some gravats are a unopenable door. It is north from the carrière and south from the ville.
Understand "gravat" as the gravats.
The description is "[if closed]Des gravats semblent avoir été entassés ici volontairement, comme pour barricader le passage. Je dois trouver un moyen de les enlever[else]Les gravats sont maintenant éparpillés par terre, rouvrant un passage scellé depuis des millénaires[end if].".

Instead of opening the closed gravats, try attacking the gravats.

Chapter 1 - Pioche

The pioche is a female thing in the carrière.
The description is "Une pioche archaïque en pierre, qui ne semble pas avoir bougé depuis des centaines d'années. Elle ne semble pas solide.".

Instead of attacking the closed gravats:
	if the player carries the pioche
	begin;
		say "Je donne un vigoureux coup afin de déblayer le passage. Mais ce coup fut le dernier pour mon instrument, qui rendit l[']âme. Il va falloir trouver autre chose.";
		remove the pioche from play;
	else;
		say "Les gravats ne bougent pas d'un pouce.";
	end if.
	


Part 7 -  Catacombes

The catacombes is west from the carrière. It is dark. It is in terre. "La caverne est à présent artificielle, et le mur comporte de nombreuses niches, contenant apparemment… des ossements. [italic type]Ils [roman type]devaient mettre leurs morts ici. Je sais que les morts sont innofensifs, mais ce lieu ne me rassure guère.".
The printed name is "Dans des catacombes".

Rule for printing the description of a dark room when the location is carrière:
	say "Renforcée par l'obscurité d'encre, une lueur fantômatique me dévisage. Rien de très rassurant…[line break]" instead.

Rule for printing the announcement of darkness when the location is carrière:
	say "L'obscurité revient, et avec elle l[']étrange lumière.".

Chapter 1 - Ossements

Some ossements are scenery in the catacombes.
Understand "os" as the ossements.
The description is "De nombreuses piles d'os sont entassées dans les cavités aux murs. Il vaut mieux ne pas les profaner.".

Chapter 2 - Crâne

The crâne de cristal is a thing in the catacombes. "Un crâne est posé sur un piédestal".
Understand "crane" as the crâne.
The description is "Ce crâne est fait du même cristal que ceux de la carrière. Il a été confectionné avec une précision redoutable et un réalisme aberrant. Il n'a pas dû être posé ici par hasard.".

After taking the crâne for the first time:
	say "Comme je me saisis du crâne, une voix résonne dans toute la cavité. Elle semble émaner de la relique :";
	wait for any key;
	say "[line break]« Qui ose profaner cet endroit sacré ? »";
	wait for any key;
	say "[paragraph break]Pétrifié par la peur, je parviens néanmoins à prononcer ces mots :[line break]« Je suis ici par la volonté de la Déesse du Ciel, auprès de qui j'ai reçu la mission de défaire le Démon de la Terre. » Je ne sais pas comment cet esprit va réagir. Ici, on ne doit pas connaître la Déesse.";
	wait for any key;
	say "[line break]« Si tel est le cas, tu auras besoin de mon aide. Mon esprit réside depuis fort longtemps dans un sanctuaire de la Déesse. Ce crâne est un lien physique qui me lie encore à ce monde. Prends-le, et viens à ma rencontre. »";
	wait for any key;
	say "[paragraph break]La voix s[']éteint. Je ne sais pas je peux lui faire confiance — comment quelqu'un ayant vécu dans les entrailles de la Terre peut-il vénérer la Déesse ? —, mais je n'ai pas trop le choix. Allons-y.".


Part 8 - Ville

The ville is in terre. The ville is dark. "Le passage débouche sous un haut dôme rocheux. Comme je balaie l'endroit de ma lampe, je découvre de nombreuses cavernes creusées dans les parois ; ce peuple devait être troglodyte. À l'ouest, l'une des grottes attire mon attention : l'entrée est plus large et il demeure des vestiges de décorations. Je devrais explorer.".
The printed name is "Sous un dôme de pierre".

Chapter 1 - Puits

The puits is a fixed in place thing in the ville. "Il y a un puits au centre.".
The description is "Un étrange trou a été creusé dans le sol, et une sorte de monte-charge permet de descendre. Je me penche pour voir ce qu'il y a au fond, mais je ne distingue rien dans les profondeurs insondables[first time]. J'y lance un caillou et attends longuement. Rien, aucun son ne se fait entendre[only]. Il doit être profond.".
The puits can be fragile. It is fragile.

Instead of entering the puits, try going down.

Instead of going down from the ville when the puits is fragile:
	say "J'entre dans le monte-charge et commence à descendre. La poulie grince affreusement, et laisse croire que tout le mécanisme va se briser d'un moment à un autre.";
	wait for any key;
	say "Mais il reste intact ; c'est la corde qui rompt. S'ensuit alors une longue chute semblant interminable.";
	wait for any key;
	say "Mais tout a une fin. Je vois venir la mienne.";
	wait for any key;
	end the story saying "Vous êtes mort !".

Before going down from the ville when the puits is not fragile:
	say "Malgré l'air fragile du monte-charge, je décide de l'utiliser. Je descends, doucement afin de ne rien casser. La descente est longue, très longue. Enfin, une lueur se fait voir. J'arrive.";
	wait for any key.


Part 9 - Antichambre

The antichambre is west from the ville. It is dark. It is in terre. "Contrairement à ce que l'on pourrait croire depuis l'extérieur, la cavité a été aménagée et le sol est dallé. D'ailleurs, quelque chose y a été gravé. Je peux continuer à l'est.".
The printed name is "À l'entrée d'une caverne".

Instead of going inside from the antichambre, try going east.

Chapter 1 - Dalles

Some dalles are scenery in the antichambre.
Understand "dalle" or "sol" as the dalles.
The description is "Sur les dalles recouvrant le sol, une inscription. Elle indique : « [italic type]Ce lieu de culte est dédié à notre divinité. Ce lieu de culte possède un jumeau, et tout ce qui sera fait par notre divinité dans ce jumeau le sera aussi dans ce lieu de culte. À jamais.[roman type] » [if we have examined the dalles]N'ai-je pas déjà vu ce texte quelque part ?[else]Étrange… Que cela peut-il bien signifier ?[end if]".

Instead of reading the dalles, try examining the dalles.

Chapter 2 - Porte

The portet is a female door. The portet is west from the antichambre and east from the lieu de culte.
The printed name is "porte".
Understand "porte" as the portet.
The description is "Une simple porte de pierre brute. Pourtant, une sorte d[']énergie semble la parcourir.".

Carry out opening the portet:
	now the portec is open.

Carry out closing the portet:
	now the portec is closed.


Part 10 - Lieu de culte

The lieu de culte is dark. It is in terre. "Une petite pièce hexagonale, n'ayant rien de particulier. C'est donc ça, leur lieu de culte ? Pourtant, je sens qu'il y a quelque chose ici.".
The printed name is "Un lieu de culte".

Instead of exiting from the lieu de culte, try going east.

Chapter 1 - Table

The tablet is a female supporter in lieu de culte.
The printed name is "table".
Understand "table" as the tablet.
The description is "Une sorte de table de pierre trônant au centre de la pièce. Tout comme pour la porte, on ressent une sorte d[']énergie la parcourir quand on la touche.".

Chapter 2 - Coffre

The coffret is a closed unopenable fixed in place container on the tablet.
The printed name is "coffre".
Understand "coffre" as the coffret.
The description is "Un coffre solide en pierre, au lourd couvercle. De même, une sorte d[']énergie semble la parcourir. Le coffre est [if open]ouvert[else]fermé[end if].".

Instead of opening the closed coffret, say "Impossible à ouvrir.".
Instead of opening the open coffre, say "Il est déjà ouvert.".

Carry out inserting the lampe into the coffret for the first time:
	now the mur is melted;
	now the boussole is in templesoi.

Carry out inserting the lampe into the coffret:
	now the lumière is in the coffrec.

Carry out taking the lampe:
	remove the lumière from play.


Part 11 - Mâchoires

The mâchoires is in terre. "Il fait ici une chaleur torride. Cet endroit est n'est que lave et roc. Je dois être au centre de la Terre. Je ne peux plus remonter ; le seul chemin est d[']énormes mâchoires aux dents acérées[if the dents are closed], mais hermétiquement closes[end if]. Le Démon de la Terre."
The printed name is "Devant d[']énormes mâchoires".
Down from the ville is the mâchoires. Up from the mâchoires is nowhere.

Chapter 1 - Dents

Some dents are a female unopenable door. It is inside from the mâchoires and outside from the gueule.
The printed name is "mâchoires".
Understand "dent", "machoire" or "machoires" as the dents.
The description is "[if closed]D[']énormes dents grises, acérées, me barrent la route[else]Les mâchoires sont ouvertes[end if]. J'imagine que je devrais pénétrer dans le corps du monstre.".

Instead of attacking the closed dents:
	Unless the player carries the épée
	begin;
		say "Je ne parviendrai à rien avec mes mains minuscules.";
	else;
		say "Confiant, la Lame céleste à la main, je frappe. Un éclair lumineux, un cri de douleur innommable, les mâchoires sont maintenant ouvertes. Je dois maintenant pénétrer dans la bête…[line break]";
		now the dents are open;
	end unless.

Part 12 - Démon

The démon is  a region. The démon is in terre. Index map with room-colour of démon set to "Dark Red".

Emerging is a scene. Emerging begins when the player is in gueule for the first time. Emerging ends sadly when the time since emerging began is 15 minutes. Emerging ends happily when the coeur is off-stage.

When emerging begins:
	say "Une profonde voix grave emplie de haine résonne alors : « Je t'attendais. »";
	wait for any key;
	say "[line break]Je frissonne malgré moi, et resserre ma prise sur la Lame.";
	wait for any key;
	say "« Tu ne peux rien contre moi. Tu seras le premier à assister à la dislocation du monde. »";
	wait for any key;
	say "[line break]La voix s[']éteint, mais résonne encore dans mon esprit. Soudain, le sol tremble, et je sens le Démon monter.";
	wait for any key;
	say "[line break]Je dois le vaincre avant qu'il n'atteigne la surface de la Terre !".

Every turn during emerging:
	say "[one of]Une secousse se fait ressentir[or]Le Démon de la Terre continue son ascension mortelle[or]La surface se rapproche [purely at random] !".

When emerging ends happily:
	say "Je brandis la Lame et frappe la pierre de toutes mes forces ; elle vole en éclats. Le Démon est secoué de toute part alors qu'un flash lumineux m'aveugle.";
	wait for any key;
	say "Toute la réalité s'effondre autour de moi. Serais-je en train de mourir ?";
	wait for any key;
	say "[line break]Quand je reprends mes esprits, je me trouve devant un château de cristal d'une beauté éblouissante. La Déesse m'attend.";
	wait for any key;
	say "[italic type]Tu as réussi. Je te remercie d'avoir vaincu le Démon de la Terre. Mais ce n'est pas fini : il te reste une mission à accomplir. Reviens à toi, et je te montrerais la voie…[roman type]";
	wait for any key;
	end the story finally saying "À suivre…".

When emerging ends sadly:
	say "Il y a soudain une énorme secousse, ainsi qu'un bruit de tonnerre. Il a émergé. Tout est fini. Il détruira bientôt le monde.";
	wait for any key;
	end the game saying "Vous avez perdu !".

Chapter 1 - Gueule

The gueule is in démon. The gueule is dark. "Rien n’est organique, tout semble à de la roche sombre. L'air ici est vicié, il pique les yeux et irrite la gorge, mais je ne peux pas rebrousser le chemin. Je dois trouver le moyen de vaincre le Démon. Le passage continue au sud.".
The printed name is "Dans la gueule du Démon".

Instead of exiting in gueule during emerging, say "La gueule s'est refermée, il n'y a plus moyen de sortir. Avançons.".

Chapter 2 - Conduit

The conduit is south from the gueule. It is dark. It is in démon. "Je suis maintenant dans une sorte de conduit. Le sol est recouvert d'une sorte de liquide rouge luminescent et visqueux. Je peux continuer au sud-ouest, au sud et à l'est.".
The printed name is "Le long d'un conduit".

Section 1 - Liquide

The liquide is backdrop in the conduit and in glande.
The description is "Un liquide visqueux et dégoûtant. N'y touchons pas.".

Chapter 3 - Glande

The glande is southwest from the conduit. It is dark. It is in démon. "Une sorte de cavité où le liquide s'amasse pour coaguler en une sorte de pierre rouge sombre et luisante. Qu'est-ce donc ?".
The printed name is "Dans une petite cavité".
 
Section 1 - Coeur

The coeur is a female fixed in place thing in the glande.
The printed name is "pierre".
Understand "pierre" as the coeur.
The description is "Une étrange pierre rouge, dégageant une aura maléfique.".

Instead of attacking the coeur for the first time:
	say "Je brandis la Lame et frappe la pierre de toutes mes forces. Le Démon a un sursaut qui fait trembler tout son corps, et la pierre s'enfonce dans ses tissus pour disparaître.";
	let the destination be a random room in démon;
	now the coeur is in destination.

Instead of attacking the coeur for the second time:
	say "Je brandis la Lame et frappe la pierre de toutes mes forces. Le Démon a un sursaut qui fait trembler tout son corps, et la pierre s'enfonce dans ses tissus pour disparaître.";
	let the destination be a random room in démon;
	now the coeur is in destination.

Instead of attacking the coeur for the third time:
	remove the coeur from play.

Chapter 4 - Estomac

The estomac is south from the conduit. It is in démon. "Cet endroit est rempli de lave. Je ne peux pas continuer.".
The printed name is "Dans ses entrailles".

Chapter 5 - Poumon

The poumon is east from the conduit. It is dark. It is in démon. "Ici, les parois sont parsemées de trous, et chacun d'eux renvoie de l'air chaud et fétide. Je dois me trouver dans le système respiratoire.".
The printed name is "À l'intérieur du poumon".



Book 3 - Ciel

Ciel is a region.
Index map with room-colour of ciel set to "Sky Blue".

Nuage is a region. Nuage is in the ciel.

Instead of going nowhere from the nuage:
	If the player is not foggy, say "La plaine blanche s[']étend jusqu[']à l'horizon, rase. Inutile de continuer dans cette direction.";
	else try looking.

To say Fog:
	say "Dans le brouillard".

To say fog:
	say "Un brouillard épais s'est soudainement levé, obstruant la vue à moins d'un mètre. Que peut bien être la raison d'un tel changement climatique ? Je n'ai maintenant plus aucuns repères[unless the room inside from the location is nothing] mais il semblerait que je puisse entrer[end if]".

Part 1 - Hall

The hall is a room in ciel. "Une vaste pièce étrange[unless the esplanade is visited], sans issues apparentes,[end unless] dont les murs semblent construits de lumière et solides comme le vent. Le sol dallé est transparent, mais je n'arrive pas à distinguer sur quoi est bâti l[']édifice, le cristal étant dépoli. Je n'ai jamais vu un endroit aussi beau.".
The printed name is "Dans [one of]un[or]le[stopping] hall lumineux".

Instead of going up from the hall when the stèle is not rang, say "Je ne peux pas aller dans cette direction.".
Instead of going outside when the esplanade is not visited, say "Je ne peux pas aller dans cette direction.".

Index map with the hall mapped east from templese.

Chapter 1 - Murs

Some murs are scenery in the hall.
The description is "Des murs de la matière la plus pure, lumineuse. On aurait dit qu'ils ont été construit à partir de rayons.".

Chapter 2 - Sol

The sol is scenery in the hall.
Understand "dalle", "dalles" or "cristal" as the sol.
The description is "Des dalles de cristal ornent le sol. Elles sont sobres mais magnifiques.".

Chapter 3 - Stèle

The stèle is a female fixed in place thing in the hall. "Une stèle trône au centre.".
Understand "stele", "caracteres", "caractere", "inscription", "inscriptions" or "cristal" as the stèle.
The description is "Un monument imposant, également de cristal, et portant des inscriptions gravées. Je ne sais pas de quelle langue il s'agit et je ne comprends pas ces caractères courbes et entrelacés, mais ils ont quelque chose de familier…[line break]".
The stèle can be rang. It is not rang.

Instead of reading the stèle:
	say "Les lettres semblent se mouvoir devant mes yeux et on aurait dit qu'elles essayaient de me transmettre un message… Non, mon imagination me joue des tours.";
	display the boxed quotation "Sonne.".

Instead of attacking the stèle for the first time:
	now the stèle is rang;
	now the escalier is in the hall;
	say "Je frappe la stèle, et un son pur s'en échappe pour résonner dans toute la pièce. Un vent venu de nulle part se lève, et un escalier formé de rayons apparaît. Je ne sais pas où cela peut me mener, mais je n'ai pas trop le choix.".

Instead of attacking the stèle, say "Elle laisse échapper un son pur.".

Chapter 4 - Escalier

The escalier is a scenery.
The description is "Un étrange escalier fait de rayons lumineux. Pourtant, il ne m'inspire pas de crainte.".

Part 2 - Balcon

The terrasse is up from the hall. It is in ciel. "[if the player is foggy][fog][else]Après avoir gravi l'escalier, je débouche à l'air libre. Je suis sur une terrasse, au sommet du bâtiment, une tour. D'ici, on peut admirer le paysage à des lieues à la ronde : une plaine blanche, brillante, immaculée, s[']étendant à perte de vue. Çà et là, on peut distinguer des bâtisses, semblables à des mirages. Cet endroit est beau[end if].".
The printed name is "[if the player is foggy][Fog][else]Sur une large terrasse[end if]".

Chapter 1 - Déesse

The déesse is a woman in the terrasse. "Une femme dotée d'une aura me domine de toute sa hauteur."
The printed name is "Déesse du Ciel".
The indefinite article is "la".
Understand "femme", "dame", "deesse" or "deesse du ciel" as the déesse.
The description is "Une personne plus haute que la normale à la peau dénuée d[']écailles et à l'abondante chevelure blonde me regarde fixement de ses yeux bleus comme des saphirs. Elle porte une ample robe blanche et est nimbée d'une aura lumineuse. Une étonnante sérénité émane d'elle.".

Instead of talking to the déesse for the first time:
	say "[italic type]Enfin.";
	wait for any key;
	say "[roman type]De nouveau, la voix cristalline résonne dans ma tête et y reste un moment avant de se dissiper. Je demeure muet, avant de finalement me reprendre :";
	wait for any key;
	say "[paragraph break]« Où suis-je ? Et qui êtes-vous ? »";
	wait for any key;
	say "[line break][italic type]Tu ne comprends toujours pas ? Je suis la Déesse du Ciel et ce que tu contemples est mon royaume. C'est moi qui t'ai fait venir ici.";
	wait for any key;
	say "[line break][roman type]Elle n'a pas remué les lèvres. La voix que j'entends est douce, mais une mystérieuse force s'en dégage. Je réponds :";
	wait for any key;
	say "[paragraph break]« Dans ce cas, qu'attendez-vous de moi ? »";
	wait for any key;
	say "[line break][italic type]De l'aide. Je suis impuissante face au Démon.";
	wait for any key;
	say "[line break][roman type]La phrase sonne toujours aussi limpidement en moi, mais son sens met du temps à se frayer dans mon esprit. La Déesse, impuissante ?";
	wait for any key;
	say "[line break]« Si vous n[']êtes pas capable de le vaincre, comment ferais-je, moi ? Je ne suis pas un héros, encore moins un dieu ! »";
	wait for any key;
	say "[line break][italic type]Je vainquis jadis le Démon de la Terre, qui sombra alors dans un sommeil de cent mille ans, au cours desquels il a pansé ses plaies. Pire, il s'est renforcé. Maintenant, je ne peux plus avoir d'influence sur le monde d'en bas, et seul quelqu'un d'extérieur à notre conflit peut agir.";
	wait for any key;
	say "Toi.";
	wait for any key;
	say "[roman type]« Très bien, il s'agit de la prophétie, après tout, me résigné-je Que dois-je faire ? »";
	wait for any key;
	say "[line break][italic type]Trouve les sanctuaires qui parsèment mon royaume. Chacun d'eux renferme un pouvoir immense que tu pourras utiliser. Va maintenant.";
	wait for any key;
	say "[line break][roman type]« Attendez… » Trop tard : il y a soudain un éclair aveuglant, et je me retrouve sur la plaine blanche. Bon, tâchons de trouver l'un de ces sanctuaires.";
	now the initial appearance of the déesse is "La Déesse du Ciel me domine de toute sa hauteur.";
	wait for any key;
	now the player is in the esplanade.

Instead of talking to the déesse, say "[italic type]Vite, le temps nous est compté.[roman type][line break]".


Part 3 - Esplanade

The esplanade is a outside from the hall. It is in nuage. "[if the player is foggy][fog][else]Je suis en extérieur. Le soleil brille fort, mais il n'est pas aveuglant. Tout autour de moi, la plaine blanche, semblable à un immense nuage, dans toutes les directions. Au centre de l'esplanade, une tour se dresse vers le ciel - celle où j'ai rencontré la Déesse.[line break]Quatre bâtiments se dressent sur l'horizon : au nord-ouest, au nord-est, au sud-est et au sud-ouest[end if].".
The printed name is "[if the player is foggy][Fog][else]Sur une large esplanade[end if]".

Chapter 1 - Autel

The autel is a supporter in the esplanade. "Il y a un petit autel près de moi."
The description is "Un petit monument sobre et sans ornements. Une si petite chose pourrait-elle être un sanctuaire[unless activated] ? Et que pourrais-je faire pour l'utiliser[end unless] ?".
The autel can be activated. The autel is not activated.

Instead of putting the orbe2 on the not activated autel:
	say "Me rappelant de ce qu'avait fait la prêtresse, je pose l'Orbe sur l'autel. Ce dernier se met à luire, puis à briller.  Puis tout redevient comme avant. Tout en récupérant l'orbe, je me demande ce qui a changé.";
	display the boxed quotation "Retourne sur terre.";
	now the autel is activated.

Instead of going from the esplanade when the oeil is not bursted:
	say "Je m'apprête à aller plus loin, quand soudain, un mur de roche jaillit du sol et s[']élève, me barrant la route. Je recule, et l'obstacle de roche disparaît, aussi vite qu'il était apparu.".


Part 4 - Plaines

Chapter 1 - Tornade

The tornade is a female fixed in place thing. "Une effrayante tornade se dresse proche de moi.".
The description is "Un tourbillon de poussière étincelante, qui semble composée d[']énergie pure. Mieux vaut s'en éloigner.".

When play begins:
	now the tornade is in a random room in nuage.

Every turn:
	if the tornade is not in the location
	begin;
		let the way be a random direction;
		if the room way from the location of the tornade is in nuage
		begin;
			now the tornade is in the room way from the location of the tornade;
			if the player can see the tornade, say "Une tornade étincelante apparaît.";
		end if;
		if a random chance of 1 in 10 succeeds and the spirit is in nuage, now the tornade is in the location of the spirit;
	end if.

Instead of taking the tornade, say "Impossible.".

Instead of entering the tornade:
	say "Une idée folle a jailli dans mon esprit, et je me dirige maintenant dans la tornade et pénètre à l'intérieur. Je me sens emporté…[line break]";
	wait for any key;
	if a random chance of 1 in 10 succeeds
	begin;
		now the player is in the chambre;
	else;
		now the player is in a random room in nuage;
	end if.

Section 1 - Trouver la tornade - not for release

Tornading is an action out of world applying to nothing.
Understand "tornade" as tornading.

Report tornading:
	say the best route from the location to the location of tornade.


Chapter 2 - Plaine nord

The plainen is north from the esplanade. It is in nuage. "[if the player is foggy][fog][else]Elle se déroule dans toute les directions, à l'infini. Seuls points de repère, les bâtiments se situant à l'ouest — le plus proche —, au nord-est, au sud-ouest et au sud-est. La tour est au sud[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 3 - Plaine nord-est

The plainene is northeast from the esplanade and east from the plainen. It is in nuage. "[if the player is foggy][fog][else]Toujours ce même sol cotonneux et immaculé. Deux bâtiments sont près d'ici, au nord-ouest et au sud. Les deux autres semblent se fondre au ciel à l'ouest et au sud-ouest. La tour est au sud-ouest[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 4 - Plaine est

The plainee is east from the esplanade, south from the plainene and southeast from the plainen. It is in nuage. "[if the player is foggy][fog][else]La plaine blanche, semblable à un nuage. À moins que ce soit vraiment un nuage. Comment fais-je pour tenir dessus ? Il y a une sorte de temple au sud-est. D'autres bâtiments semblent posés sur l'horizon au nord, au nord-ouest et au sud-ouest. La tour est à l'ouest[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 5 - Plaine sud-est

The plainese is southeast from the esplanade and south from the plainee. It is in nuage. "[if the player is foggy][fog][else]Le soleil est éclatant, mais sa douce chaleur est apaisante et son reflet sur le sol immaculé n'est pas aveuglant. Un temple se situe à l'est. Sinon, il y en a un proche vers l'ouest. Les autres sont au nord-ouest et au nord-est. La tour, quant à elle, est au nord-ouest[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 6 - Plaine sud

The plaines is south from the esplanade, southwest from the plainee and west from the plainese. It is in nuage. "[if the player is foggy][fog][else]Le ciel est d'un bleu azur, si profond que l'on finit par s'y perdre ; si on ne se perd pas d'abord dans la plaine ! La tour est au nord, et un bâtiment a été construit au sud-ouest. Sinon, on peut voir un monument proche à l'est et dans le lointain deux autres au nord-ouest et au nord-est[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 7 - Plaine sud-ouest

The plaineso is southwest from the esplanade and west from the plaines. It is in nuage. "[if the player is foggy][fog][else]Rien de nouveau. La tour de lumière est au nord-est et un monument se dresse au sud. Deux autres bâtiments se dressent plus loin, l'un au nord et l'autre à l'est. On distingue à peine celui au nord-est[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans plaine[end if]".


Chapter 8 - Plaine ouest

The plaineo is west from the esplanade, northwest from the plaines and north from the plaineso. It is in nuage. "[if the player is foggy][fog][else]La tour où j'ai rencontrée la Déesse s[']élève à l'est, des bâtiments se dressent au sud et au nord-ouest et je distingue des édifices loin au nord-est et plus à l'est. Hormis cela, la plaine, toujours la plaine[end if]."
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".

Chapter 9 - Plaine nord-ouest

The plaineno is northwest from the esplanade, north from the plaineo and west from the plainen. It is in nuage. "[if the player is foggy][fog][else]Je suis proche de la tour, au sud-est, et d'une sorte de temple au nord-ouest. Par-delà la plaine immaculée, je peux voir deux autres bâtiments, au nord-est et au sud[end if]."
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 10 - Plaine nord nord-ouest

The plainenno is north from the plaineno and northwest from the plainen. It is in nuage. "[if the player is foggy][fog][else]Je m'approche du temple, qui se situe à l'ouest. Autrement, je me retourner sur mes pas où vers les autres bâtiments au sud et au sud-est[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 11 - Plaine ouest nord-ouest

The plaineono is southwest from the plainenno, west from the plaineno and northwest from the plaineo. It is in nuage. "[if the player is foggy][fog][else]Je me fatigue un peu du paysage monotone, mais on ne se lasse pas facilement de son étrange beauté. Des temples au nord, au sud, une tour de lumière au sud-est et, chose qui m'intrigue et m'attire, de hauts piliers au sud-ouest. Je devrais m'approcher[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 12 - Plaine ouest ouest

The plaineoo is south from the plaineono, southwest from the plaineno, west from the plaineo and northwest from the plaineso. It is in nuage. "[if the player is foggy][fog][else]Je m[']éloigne de l'esplanade, et me situe maintenant entre deux monuments au nord et au sud. Mais le plus intéressant se trouve à l'ouest : de singulières colonnes s[']élèvent vers le ciel — même si j'y suis déjà[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 13 - Plaine ouest sud-ouest

The plaineoso is south from the plaineoo, southwest from the plaineo and west from the plaineso. It is in nuage. "[if the player is foggy][fog][else]Je me situe maintenant entre deux constructions : le temple au sud-est et d'étranges colonnes dans la direction opposée. La plaine est vaste, et on finit par reconnaître le paysage : la tour et un autre édifice à l'est, un monument au nord… Tâchons de ne pas se perdre[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 14 - Plaine est est

The plaineee is southeast from the plainene, east from the plainee and northeast from the plainese. It is in nuage. "[if the player is foggy][fog][else]Je me situe plein est de la tour lumineuse et au nord d'une construction semblable à un temple. D'ailleurs, il en a d'autres, au nord, au nord-ouest et au nord-est[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 15 - Plaine est nord-est

The plaineene is east from the plainene, northeast from the plainee and north from the plaineee. It is in nuage. "[if the player is foggy][fog][else]Je ne me trouve près d'aucun bâtiment, mais je peux voir un édifice au nord et au sud, ainsi que la tour à l'ouest. Je devrais m'approcher de l'un d'eux[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 16 - Plaine nord-est nord-est

The plainenene is northeast from the plainene and north from the plaineene. It is in nuage. "[if the player is foggy][fog][else]Il y a un temple au nord-est, et la tour est au sud-ouest. Sinon, tout se perd et se confond dans l'immensité de la plaine blanche et la légère brume étincelante[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".

Chapter 17 - Plaine nord-est nord-est nord

The plainenenen is north from the plainenene. It is in nuage. "[if the player is foggy][fog][else]J'ai fait un bon bout de chemin, et me voilà maintenant à l'ouest d'un temple. Autrement, j'ai perdu mes points de repère qui se sont perdus dans la brume. Je devrais peut-être revenir[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 18 - Plaine est nord-est nord-est

The plaineenene is southeast from the plainenenen, east from the plainenene and northeast from the plaineene. It is in nuage. "[if the player is foggy][fog][else]Je suis au sud du temple le plus proche, et loin de tout le reste, que l'on peut à peine distinguer sur l'horizon. Cette plaine est immense, je vais finir par me perdre si je ne fais pas attention[end if].".
The printed name is "[if the player is foggy][Fog][else]Dans la plaine[end if]".


Chapter 19 - Plaine sud sud

The plainess is southeast from the plaineso, south from the plaines and southwest from the plainese. It is in nuage. "[if the player is foggy][fog][else]Ici, la plaine s'arrête brusquement et un précipice d'un bleu insondable me sépare d'un autre nuage. Sur celui-ci a été construit un palais immense, semblable à une vision tellement il est beau. Mais ne nous attardons pas, il y a un temple à l'ouest et un autre à l'est. Les autres édifices sont loin au nord[end if].".
The printed name is "[if the player is foggy][Fog][else]Au bout de la plaine[end if]".


Chapter 20 - Plaine sud sud-est

The plainesse is east from the plainess, southeast from the plaines and south from the plainese. It is in nuage. "[if the player is foggy][fog][else]Le nuage sur lequel je me trouve s'arrête là. De l'autre côté, sur un minuscule îlot, est dressé le plus beau bâtiment que j'ai jamais vu : les murs semblent de lumière et les toits de vent. Malheureusement, je dois me retourner : il y a un édifice au nord-est, un autre un peu plus loin à l'ouest. La tour est au nord-ouest et il y a un autre bâtiment à l'horizon au nord[end if].".
The printed name is "[if the player is foggy][Fog][else]Au bout de la plaine[end if]".


Part 5 - Temple nord-ouest

The templeno is north from the plaineono, northwest from the plaineno and west from the plainenno. It is in nuage. "[if the player is foggy][fog][else]Je me trouve maintenant devant un temple fait de cristal semblant capter la lumière afin de la restituer, encore plus brillante. Une énorme colonnade translucide accueille les visiteurs. Ce serait quasi-criminel de ne pas entrer[end if].".
The printed name is "[if the player is foggy][Fog][else]Devant un temple de cristal[end if]".

Chapter 1 - Intérieur du temple

The templenoi is inside from the templeno. It is in ciel. "Une grande pièce illuminée par le plafond transparent concentrant la lumière. Un silence absolu règne ici, hormis le son cristallin que produisent mes pas. Il doit s'agir d'un sanctuaire.".
The printed name is "À l'intérieur du temple".
Index map with templenoi mapped west from the templeno.

Section 1 - Plaque

The plaque is a female fixed in place thing in the templenoi. "Une plaque est accrochée au mur.".
The description is "Une plaque de marbre a été fixée au mur. Une inscription y a été gravée.".

Instead of reading the plaque, say "« Toi qui viens en ces lieux, agenouille-toi et recueille-toi. Tes vœux seront alors exaucés. »[line break]".

Instead of praying in the templenoi for the first time:
	say "Comme l'indique l'inscription, je me mets à prier. Au même moment, l'Orbe se met à vibrer, plus lumineuse et plus chaude. Puis tout s'arrête. Je devrais retourner sous terre, voir se qui s'est passé.";
	now the lave is solid.


Part 6 - Temple nord-est

The templene is east from the plainenenen, northeast from the plainenene and north from the plaineenene. It is in nuage. "[if the player is foggy][fog][else]Une merveille d'architecture se dresse devant moi. Semblant faite de cristal, la construction paraît d'une fragilité extrême. C'est pourtant ce qui la rend belle. Malheureusement, il n'y a pas d'entrée, et je ne pourrai pas visiter cet étrange monument[end if].".
The printed name is "[if the player is foggy][Fog][else]Devant un temple de cristal[end if]".

Instead of going inside from the templene, say "Il n'y a pas d'entrée visible, impossible de pénétrer dans le bâtiment.".

Chapter 1 - Intérieur du temple

The templenei is a room. It is in ciel. "[first time]Étrangement, je me retrouve dans ce temple, alors que je n'y étais pas auparavent. Cela doit être à cause du crâne. [only]Voci donc le sanctuaire où résiderait un spectre mystérieux. Cet endroit est lumineux, accueillant, et ne ressemble pas à la demeure d'un fantôme. Et pourtant, je sens une présence, même si je ne vois rien. L'esprit daignera-t-il se montrer[first time] ou devrais-je trouver moi-même la solution[only] ?".
The printed name is "À l'intérieur du temple".

Section 1 - Canne

The canne is a female thing.
The printed name is "canne d'argent".
The description is "Un magnifique objet en argent ciselé. Il est d'une perfection extrême, normalement impossible à atteindre. Il appartient à la Déesse, après tout.".

Instead of praying in the templenei for the first time:
	say "Comme la dernière fois, je m'agenouille et commence ma prière. Un tourbillon de vent se lève au centre de la pièce. Au centre de celui-ci, un objet apparaît.";
	wait for any key;
	say "[italic type]Voici la canne de la Déesse. Elle contient une quantité phénoménale d[']énergie, mais inutilisable tel quel.";
	wait for any key;
	say "Cherche la tornade qui court sur les plaines. Elle la libérera.";
	say roman type;
	now the canne is in the templenei.

Instead of throwing the canne at the tornade:
	say "Je me saisis fermement de la canne et la lance de toutes mes forces dans la tornade. Elle atteint son cœur, et explose dans une poussière d[']étoile scintillante. Maintenant que son énergie est libérée, voyons que faire.";
	remove the canne from play;
	now the gravats are open. 

Section 2 - Clef rouillée

The clef rouillée unlocks the coffre. It is in the templenei. It is female.
Understand "cle/clef rouillee" or "cle" as the clef rouillée.
The description is "Une petite clef rouillée, semblant sur le point de se désagréger. Elle ne semble pas provenir de l'endroit où je l'ai trouvée.".


Part 7 - Temple sud-ouest

The templeso is southeast from the plaineoso, south from the plaineso, southwest from the plaines and west from the plainess. It is in nuage. "[if the player is foggy][fog][else]Une magnifique construction, et le mot est faible. Fait d'un cristal d'une pureté impossible à atteindre, elle rend bien pâle notre architecture, dont nous sommes pourtant fiers. Un magnifique tympan surplombe l'entrée[end if]."
The printed name is "[if the player is foggy][Fog][else]Devant un temple de cristal[end if]".

Section 1 - Tympan

The tympan is scenery in templeso.
The description is "Le tympan de cristal a été soigneusement sculpté, et on peut y lire : « [italic type]Ce temple a été construit pour éclairer le chemin d'un peuple, jusqu'alors dans l'ombre. Ce temple possède un jumeau, et tout ce qui sera fait dans ce jumeau le sera aussi dans ce temple. À jamais.[roman type] » [if we have examined the dalles]N'est pas déjà vu ce texte quelque part ?[else]Je devrais chercher ce jumeau.[end if]".

Instead of reading the tympan, try examining the tympan.


Chapter 2 - Intérieur du temple

The templesoi is a room in ciel. "Un autre temple de cristal, toujours aussi beau. On ne peut s'en lasser. Devrais-je prier ?".
The printed name is "À l'intérieur du temple".

Instead of praying in the templesoi for the first time, say "De nouveau, je me mets à prier. Mais rien ne se passe.".


The portec is a female unopenable door. The portec is inside from the templeso and outside from the templesoi.
The printed name is "porte".
Understand "porte" as the portec.
The description is "Une lourde et belle porte de marbre, sobre et inébranlable. Curieusement, elle n'a pas de poignée ni de serrure.[if closed] Comment l'ouvrir ?[else]Elle est maintenant ouverte. Étrange…[line break][end if]".

Instead of opening the closed portec, say "Sans poignée ni serrure ? Impossible.".

Section 1 - Table

The tablec is a female supporter in the templesoi.
The printed name is "table".
Understand "table" as the tablec.
The description is "Une table de marbre finement sculptée trône au centre de la pièce. C'est une véritable œuvre d'art.".

Section 2 - Coffre

The coffrec is a closed transparent openable fixed in place container on the tablec.
The printed name is "coffre".
Understand "coffre" as the coffrec.
The description is "Une sorte de coffre transparent, semblant capter la lumière et la concentrer vers un point précis du mur. [if the lampe is in the coffret]Le puissant rayon, jaillissant de nulle part, y a créé un trou[else]Malheureusement, le rayon n'est pas assez puissant et rien ne se passe[end if]. Le coffre est [if open]ouvert[else]fermé[end if].".

Carry out opening the coffrec:
	now the coffret is open.

Carry out closing the coffrec:
	now the coffret is closed.


Section 3 - Mur

The mur is scenery in templesoi.
The description is "Le mur est entièrement en cristal[if melted]. Il a été percé par la lumière[end if].".
The mur can be melted.

Section 4 - Lumière

The lumière is scenery.
Understand "lumiere", "rayon" or "faisceau" as the lumière.
The indefinite article is "de la".
The description is "[if the lampe is in the coffret]Un puissant rayon de lumière, jaillissant de nulle part, se dirige vers le mur. Il a fait fondre une partie de ce dernier[else]Un minuscule faisceau se réduisant à un point au mur. Il n'est pas assez puissant et rien ne se passe[end if].".

Section 5 - Boussole

The boussole is a female thing.
The description is "Un étrange dispositif contenant une aiguille en pierre bleue. Celle-ci [if the location is not in nuage]pointe vers la sortie[else if the location is the hidden place]tourne sur elle-même à grande vitesse, affolée[else]indique une direction : [the best route from the location to the hidden place][end if]. Sur le revers, il est marqué : « [italic type]Prie[roman type] »."

The hidden place is a room that varies.
When play begins:
	now the hidden place is a random room in nuage.

Instead of praying in the hidden place for the first time:
	now the puits is not fragile;
	say "Je me mets à prier, à l'endroit indiqué par la boussole. Je sens que quelque chose a maintenant changé.".


Part 8 - Temple sud-est

The templese is south from the plaineee, southeast from the plainee, east from the plainese and northeast from the plainesse. It is in nuage. "[if the player is foggy][fog][else]Un immense temple translucide évoquant la pureté même. Sa seule vision suffit à me faire frissonner tellement il est sublime. J'ai hâte d'entrer[end if]."
The printed name is "[if the player is foggy][Fog][else]Devant un temple de cristal[end if]".

Chapter 1 - Intérieur du temple

The templesei is inside from the templese. It is in ciel. "Une unique et gigantesque pièce, avec au centre un piédestal accessible par un escalier.[if the épée is on the piédestal] Et au sommet…[paragraph break]Une magnifique épée est scellée dans le cristal.[end if][line break]".
The printed name is "À l'intérieur du temple".

Section 1 - Épée

The épée is a female thing.
Understand "lame/epee" as the épée.
Understand "lame/epee celeste" as the épée when the épée is handled.
The description is "Une lame blanche, veinée de bleu, à la poignée d'argent. Elle ne semble pas avoir été touchée depuis des lustres.".
The épée is on the piédestal.


Instead of pulling the épée, try taking the épée.

Instead of taking the épée when the player is not on the piédestal, say "Je dois d'abord monter sur le piédestal.".

Instead of taking the épée when the Nâga is not in mâchoires, say "Je tire de toute mes forces, mais elle ne bouge pas d'un pouce.".

After taking the épée:
	now the player is foggy;
	say "Je tire de toute mes forces, mais l[']épée glisse comme si elle sortait de l'eau. Je suis maintenant prêt à vaincre le Démon.";
	now the printed name of the épée is "Lame céleste";
	now the indefinite article of the épée is "la";
	display the boxed quotation "Va aux ruines.".

Instead of dropping the épée, say "J'en ai besoin.".

Section 2 - Piédestal

The piédestal is an undescribed enterable supporter in the templesei.
Understand "piedestal" as the piédestal.
The description is "Un magnifique ouvrage de cristal bleu[if the épée is on the piédestal] avec planté à l'intérieur une magnifique épée blanche ; l['][italic type]Épée céleste[roman type], à en croire la gravure sur le piédestal[end if]."


Part 9 - Ruines

Chapter 1 - Extérieur des ruines

The ruines is southwest from the plaineono, west from the plaineoo and northwest from the plaineoso. It is in ciel. "[if the player is foggy]Malgré le brouillard, je peux voir d[else]D[end if]e majestueuses colonnes se dress[if the player is foggy]ant[else]e[end if] vers le ciel. Malheureusement, tout est en ruines ici, et je sens une aura maléfique nimber cet endroit.".
The printed name is "Devant des ruines".

After going to the ruines:
	display the boxed quotation "Le Démon de la Terre à réussi à s'introduire
	dans mon royaume en violant ce sanctuaire.
	Il faudra le repousser ici avant de pouvoir 
	le vaincre dans son domaine.
	Tout repose sur toi.";
	continue the action.

Section 1 - Tentacule

The tentacule is inside from the ruines and outside from the ruinesi. It is an unopenable locked door. "Une sorte de gigantesque tentacule [if closed]gesticule dans tous les sens, me barrant la route[else]gît à terre, coupé en deux[end if]."

The description is "Un long tentacule pourvu d'excroissances rocheuses sort du sol. Elle doit être l'appendice de quelque chose de beaucoup plus gros se terrant dans les ruines.".

Instead of going inside from the ruines when the tentacule is closed:
	say "Dans ma volonté de continuer, je m'approche de l'horreur qui me bloque le passage. Celle-ci n'a pas de pitié, et se saisit de moi.";
	wait for any key;
	say "Je n'ai pas le temps de réagir, l[']étau se ressert et la créature me broie.";
	wait for any key;
	end the story saying "Vous êtes mort !".

Instead of opening the closed tentacule:
	say "Dans ma volonté de continuer, je m'approche de l'horreur qui me bloque le passage. Celle-ci n'a pas de pitié, et se saisit de moi.";
	wait for any key;
	say "Je n'ai pas le temps de réagir, son étau se ressert et la créature me broie.";
	wait for any key;
	end the story saying "Vous êtes mort !".

Instead of attacking the closed tentacule:
	if player carries the épée
	begin;
		say "Je lève la Lame bien haut avant de la laisser retomber violemment sur le tentacule. Il y a un flash aveuglant et l'immonde chose est coupée en deux, libérant le passage.";
		now the tentacule is open;
	else;
		say "Je frappe le tentacule avec mes poings, espérant qu'il va se rétracter.";
		wait for any key;
		say "Pourtant, il s'allonge et me domine maintenant de toute sa hauteur. Je n'ai que le temps de le voir s'abattre sur moi";
		wait for any key;
		end the game saying "Vous êtes mort !";
	end if.

Chapter 2 - Intérieur des ruines

The ruinesi is in ciel. "Ici, il n'y a plus de cristal. Seule la roche grise et froide m'accueille. Cet endroit a été souillé par la corruption du Démon. Je n'ai pas envie de rester longtemps ici[unless the fissure is part of the mursr]. Le plafond est effondré[end unless].".
The printed name is "Entre des murs de pierre".

Section 1 - Murs

Some mursr are scenery in the ruinesi.
Understand "mur", "murs" or "pierre" as the mursr.
The description is "Des murs froids de pierre, semblant complètement décalés dans ce monde de lumière. [if the fissure is part of the mursr]Une fissure inquiétante lézarde l'un d'eux[else]Le plafond est effondré, et un large flot de lumière pénètre en une large cascade[end if].".

Instead of attacking the mursr when the fissure is part of the mursr, try attacking the fissure.

Section 2 - Fissure

The fissure is part of the mursr. It is female.
The description is "Une large fissure sur l'un des murs, semblant mettre en péril le bâtiment entier.".

Instead of attacking the fissure for the first time:
	say "De toutes mes forces, j'essaie d[']élargir la fissure. Elle résiste, mais s'allonge tout de même jusqu'au plafond.";
	The walls break in one turn from now.

Instead of opening the fissure, try attacking the fissure.
Instead of attacking the fissure, say "Elle est déjà bien assez grande comme ça.".

At the time when the walls break:
	if the player is in the ruinesi
	begin;
		say "Fragilisé par l[']énorme fissure, le plafond s[']écroule. Sur moi. C'est la fin.";
		wait for any key;
		end the story saying "Vous êtes mort !";
	end if;
	remove the fissure from play;
	now the eye is open.


Section 3 - Œil

The eye is a thing in the ruinesi. "[if the eye is closed]Un énorme œil semble incrusté dans un mur[else]La lumière semble avoir un effet négatif sur l[']œil[end if].".
The printed name is "œil".
Understand "oeil" as the eye.
The description of the eye is "[if closed]Cet œil est résolument fermé et ne laisse voir que son épaisse paupière écailleuse[else]La lumière du ciel semble brûler l[']œil, et de la fumée s'en dégage[end if]."
The eye can be open or closed. The eye is closed.

Instead of opening the closed eye, say "À peine effleuré-je la peau écailleuse qu'une vive brûlure se ressent. Je retire aussitôt ma main, faisant cesser la douleur.".

Instead of closing the open eye, say "Je devrais profiter de sa faiblesse pour frapper !".

Instead of attacking the eye:
	if the eye is closed
	begin;
		say "Je brandis la Lame céleste et frappe l[']œil, mais celle-ci ripe sur les écailles. La chose ne semble pas avoir été dérangée.";
	else;
		say "Profitant de la faiblesse de l[']œil, je parviens à planter l[']épée dans le globe oculaire. Il finit par se désagréger et disparaître, révélant ainsi une sorte de socle.";
		remove the eye from play;
		now the socle is in the ruinesi;
	end if.

Section 4 - Socle

The socle is scenery in ruinesi.
The description is "Une plaque de pierre translucide, avec une forme en creux sculptée. À quoi peut-elle servir ?".

Instead of inserting something into the socle, say "Ça ne rentre pas.".

Instead of inserting the épée into the socle:
	say "L[']épée s'adapte parfaitement au socle, et elle commence à luire. Puis elle devient transparente, de plus en plus, avant de finalement disparaître. Où est-elle passée ?";
	now the Nâga carries the épée.

Instead of inserting the clef rouillée into the socle:
	say "La clef s'adapte au socle, même si l'emplacement est bien trop grand. Elle commence à luire, devient transparente avant de finalement disparaître. Où est-elle passée ?";
	now the Nâga carries the clef rouillée.



Part 10 - Chambre

The chambre is a room in ciel. "La tornade m'a porté dans un lieu étrange : je me trouve maintenant dans une pièce lumineuse semblant être une chambre à coucher. Il y a ici un lit à baldaquin et surtout, un tableau au mur. Serais-je… dans la chambre de la Déesse du Ciel ? Dans tous les cas, une certaine nostalgie se dégage de l'endroit.[line break]Il semblerait que je puisse quitter l'endroit par une sortie aveuglante.".
The printed name is "Dans une chambre à coucher".

Instead of exiting from chambre, now the player is in the location of the tornade.

Chapter 1 - Lit

The bed is scenery in the chambre.
The printed name is "lit".
Understand "lit" as the bed.
The description is "Le lit est parfaitement fait, on aurait même dit qu'il n'a jamais été occupé. Les tentures colorées de nombreuses teintes de bleu semblent extrêmement précieuses, de grande valeur. Où ai-je bien pu atterrir ?".

Chapter 2 - Tableau

The tableau is scenery in the chambre.
The description is "Un œuvre de maître, portrait de deux enfants :  une petite fille blonde et un garçon, plus grand et brun. Tous deux sourient et se tiennent par la main ; ils ont l'air heureux. Il y a une petite plaque fixée sous la toile indiquant [italic type]En souvenir de la personne qui fut autrefois un frère, et non le Léviathan[roman type].".

Carry out examining the tableau for the first time:
	increment the score.


Volume 3 - Release
	
The story headline is "Une fiction interactive".
The story genre is "Fantasy".
The release number is 1.
The story description is "Le Démon de la Terre s'est éveillé et, accomplissant une ancienne prophétie, vous vous êtes rendu sur une île déserte pour y chercher l'Orbe Étincelante, seule arme capable de le vaincre. Après avoir affronté de nombreux périls et trouvé l'artéfact divin, vous retournez maintenant parmi les vôtres, espérant qu'il n'est pas trop tard et que l'on pourra vous indiquer que faire à présent...
Ce jeu est la suite du ''Temple Nâga''.".
The story creation year is 2011.

Release along with cover art and the source text.

Book 1 - Commandes

Part 1 - Aide

After printing the banner text:
	say "[first time]Tapez « aide » pour obtenir plus d'informations sur le jeu ou si vous jouez à une fiction interactive pour la première fois.[line break][unicode 8656] Avant de commencer, assurez-vous d'avoir joué au premier volet, [italic type]le Temple Nâga[roman type] ![line break]N. B. Ce jeu a été fini à la hâte (pour ne pas dire à la bourre) pour pouvoir être dans le concours et n'a donc pas pu être entièrement testé. Il est probable qu'il contienne des bugs, mais il est très largement finissable.[only]"

Understand "aide", "infos", "info", "informations" or "information" as a mistake ("Ce jeu est une fiction interactive ; il se joue en tapant des commandes à l'infinitif telles que « voir », « prendre [italic type]objet[roman type] », etc. (pour plus d'informations, consultez [italic type]www.ifiction.free.fr[roman type]).[line break]Tapez « licence » pour l'obtenir et « auteur » pour en savoir plus sur l'auteur.").

Part 2 - Licence

Understand "licence" or "license" as a mistake ("[bold type]Pour faire bref :[line break][roman type]Ce programme est un logiciel libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.[paragraph break]Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.[paragraph break][bold type]Pour faire long, [roman type]consultez : [italic type]http://www.gnu.org/licenses[roman type].").

Part 3 - Auteur

Understand "credits", "credit", "auteur" or "auteurs" as a mistake ("Il n'y a pas encore grand-chose à dire sur moi, si ce n'est que mon pseudonyme est Natrium. Ce jeu est l'une des premières fictions interactives que j'ai faites, il y aura donc probablement une autre version avec un lien vers un site ou autre. Patience… (mais vous pouvez quand même me contacter à cette adresse : [italic type]natrium729@gmail.com[roman type])").

Part 4 - Test

Test lave with "s / parler à prêtresse / x orbe / écouter orbe / x stèle / lire stèle / frapper stèle / haut / parler / poser orbe sur autel / écouter orbe / sortir / bas / e / pr stalagmite / encore / crever oeil / o / se laver / écouter orbe / no / no / entrer / prier / écouter orbe / e / n".

Test crane with "pr lampe / ouvrir sac / pr cristal / insérer cristal dans lampe / n / pr pioche / attaquer gravats / o / pr crâne / écouter orbe / prier /pr canne /sortir / * tu dois trouver la tornade".

Test boussole with "écouter orbe / poser crâne / e / n / o / o / écouter orbe".

Test final with "sortir / est / est / nord-est / prier / écouter orbe / pr lampe / e / e / bas / écouter orbe / entrer / monter sur piédestal / pr épée / sortir / sortir / o /o / o / o / no / attaquer tentacule / entrer / ouvrir fissure / sortir / entrer / attaquer oeil / insérer lame dans socle / écouter orbe / attaquer dents / entrer".