# Entre Terre et Ciel #

*Le Démon de la Terre s'est éveillé et, accomplissant une ancienne prophétie, je me suis rendu sur une île déserte pour y chercher l'Orbe Étincelant, seul arme capable de le vaincre. Après avoir affronté de nombreux périls et trouvé l'artefact divin, je retourne maintenant parmi les miens, espérant qu'il n'est pas trop tard et que l'on pourra m'indiquer que faire à présent…*

## Jouer ##

*Entre Terre et Ciel* est une fiction interactive, deuxième volet de la trilogie commencée avec *le Temple nâga*, et a fini troisième au [concours de fiction interactive 2011](http://ifiction.free.fr/index.php?id=concours&date=2011). Elle se joue en tapant des verbes à l'infinitif afin de guider le héros dans sa quête.

Si vous voulez essayer, ce n'est pas ici qu'il faut regarder ! Vous pouvez jouer en ligne à la version du concours [à cet endroit](http://ifiction.free.fr/index.php?id=jeu&j=058) ; néanmoins, elle n'est vraiment pas aboutie car je l'ai terminée à la va-vite pour être dans les temps. Une autre version devrait bientôt voir le jour.

## La Source ##

La source de ce jeu est visualisable dans ce dépôt (dans le fichier « TerreCiel.inform/Source/story.ni ») afin de satisfaire votre curiosité. Elle est compilable avec la version 6L38 d'Inform 7.

Le répertoire en « -6G » contient la source de la première version d'*Entre Terre et Ciel,* écrite avec la version 6G60 d'Inform 7, qui est gardée à titre historique.

## Pour m'aider ##

Rien de plus simple : jouez ! Cela me ferait également plaisir si vous me faites part de vos commentaires/impressions/remarques. Vous pouvez me contacter via [mon site](http://ulukos.com/contact/) ou sur [le forum de la communauté francophone dédié aux fictions interactives](http://ifiction.free.fr/taverne/index.php).

Si vous trouvez une erreur dans le jeu, vous pouvez utiliser les mêmes liens que ci-dessus, ou bien l'outil de signalement de bug (menu ci-contre).