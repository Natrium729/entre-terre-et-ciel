"Entre Terre et Ciel" by Nathanaël Marion (in French)

[
Pour faire bref :
Ce code source est libre ; vous pouvez le redistribuer ou le modifier suivant les termes de la « GNU General Public License » telle que publiée par la Free Software Foundation : soit la version 3 de cette licence, soit toute version ultérieure.

Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE : sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.

Pour faire long, consultez : http://www.gnu.org/licenses
]

Include Experimental French Features by Nathanael Marion.

Include (- 
	language French
	
	<control-structure-phrase> ::=
		/a/ si ... est début |
		/b/ si ... est |
		/c/ si ... |
		/c/ à moins que |
		/d/ répéter ... |
		/e/ tant que ... |
		/f/ sinon |
		/g/ sinon si ... |
		/g/ sinon à moins que |
		/h/ -- sinon |
		/i/ -- ... |

-)  in the Preform grammar.

Volume 1 - Initialisation

Book 1 - Extensions

Part 1 - Basic Screen Effects

Include Basic Screen Effects by Emily Short.

Part 2 - Glulx Text Effects

Include Glulx Text Effects by Emily Short.

[TODO Mettre à jour le CSS de Quixe + trouver un meilleur moyen pour les message de la Déesse.]
Table of User Styles (continued)
style name	background color	color	fixed width	font weight	indentation	first line indentation	italic	justification	relative size	reversed
blockquote-style	"#FFFFFF"	"#0000FF"	false	regular-weight	0	0	false	center-justified	1	false

Book 2 - Options

Use scoring.
Le maximum score est 2.

Book 3 - Nouveautés

Part 1 - Parler

Talking to is an action applying to one visible thing.
Understand "talk to [something]", "speak to [something]", "parler a/au/aux/avec/-- [something]" or "questionner [something]" as talking to.
Understand "talk to [someone]", "speak to [someone]", "parler a/au/aux/avec/-- [someone]" or "questionner [someone]" as talking to.
Understand the command "discuter" as "parler".

Check talking to:
	si le nom n'est pas une personne, dire "Je ne peux pas parler [au noun][_]!" instead;

Check talking to:
	si le nom est le player, dire "Je me parle à moi-même pendant un moment." instead.

Report talking to:
	dire "Je n'ai rien à dire.".

Rule for clarifying the parser's choice of something (called item) while talking to:
	dire "([au item])[command clarification break]".

Does the player mean talking to a person: it is very likely.

Part 2 - Lire

Understand the command "read" as something new.
Understand the command "lire" as something new.
Reading is an action applying to one visible thing and requiring light.
Understand "read [something]" or "lire [something]" as reading.

Report reading (this is the standard report reading rule):
	dire "Il n'y [as] rien de spécial à lire [ici]." (A).

Report someone reading (this is the report other people reading rule):
	dire "[The actor] [essayes] de lire quelque chose sur [the noun]." (A).

Part 3 - Prier

Praying is an action applying to nothing.
Understand "prier" and "recueillir vous" as praying.

Report praying:
	dire "Je m'incline et commence à murmurer des paroles sacrées. Je me relève enfin après quelques secondes. Une prière ne fait jamais de mal.".

Volume 2 - Jeu

Book 1 - Introduction

Part 1 - État initial

Chapter 1 - Héros

Le Nâga est un homme privately-named. Le Nâga est dans la grand-place.
Le printed name est "moi-même".
Le Nâga peut être ensanglanté. Le Nâga est non ensanglanté.
La description du Nâga est "Je suis un jeune Nâga ordinaire, qui vient de finir mes neuf mues, et mes écailles bleues indiquent maintenant que je suis un adulte. Choisi par la prophétie, je suis censé vaincre le Démon de la Terre. Est-ce possible pour un être normal tel que moi[_]?[si ensanglanté][à la ligne]Je suis couvert de sang[fin si].".
Le player est le Nâga.

L' esprit est un homme. L' esprit est dans le hall.
Le printed name est "moi-même".
La description est "Je suis un jeune Nâga ordinaire, qui vient de finir mes neuf mues, et mes écailles bleues indiquent maintenant que je suis un adulte. Choisi par la prophétie, je suis censé vaincre le Démon de la Terre. Est-ce possible pour un être normal tel que moi[_]?[à la ligne]Je suis étrangement transparent.".
L' esprit peut être brumeux. L' esprit est non brumeux.

Chapter 2 - Orbe

Section 1 - Orbe sur Terre

L' orbe-terre est une chose à nom propre privately-named.
Le printed name est "l'Orbe Étincelant".
La description est "L'Orbe Étincelant, seule arme capable de vaincre le Démon de la Terre. Il brille de mille éclats et dégage une douce chaleur.".
L' orbe-terre peut être activé. L' orbe-terre est non activé.
Le Nâga porte l' orbe-terre.

Comprendre "orbe" comme l' orbe-terre.

Instead of dropping l' orbe-terre, dire "Pas question d'abandonner[_]!".

Report examining l' orbe-terre activé:
	afficher la citation encadrée "Écoute-moi.".

Instead of listening to l' orbe-terre activé pour la première fois:
	dire "J'approche l'Orbe de mon oreille. Tout d'abord, rien ne se fait entendre[_]; puis, petit à petit, une douce mélopée s'élève. Envoûté, je ferme les yeux.";
	attendre une touche;
	dire "Quand je les rouvre enfin, je découvre un lieu inconnu, nimbé de lumière. Où suis-je[_]?";
	attendre une touche;
	afficher la citation encadrée "Trouve-moi.";
	maintenant le player est l' esprit;
	essayer de looking.

Instead of listening to l' orbe-terre activé:
	dire "De nouveau, la douce mélopée se fait entendre. De nouveau, je ferme les yeux. Et quand je les rouvre…[à la ligne]";
	attendre une touche;
	maintenant le player est l' esprit;
	essayer de looking.

Instead of listening to l' orbe-terre activé when le player est ensanglanté, dire "J'approche l'Orbe de mon oreille. Pourtant, la mélodie ne vient pas. Où est donc le problème[_]?".

[TODO]
Before listening to l' orbe-terre activé when le player porte le crâne, maintenant l' esprit est dans le temple-nei.

Section 2 - Orbe dans le ciel

L' orbe-ciel est une chose à nom propre privately-named.
Le printed name est "l'Orbe Étincelant".
La description est "L'Orbe Étincelant, seule arme capable de vaincre le Démon de la Terre. Il brille de mille éclats et dégage une douce chaleur.".
L' esprit porte l' orbe-ciel.

Comprendre "orbe" comme l' orbe-ciel.

Instead of dropping l' orbe-ciel, dire "Pas question d'abandonner[_]!".

Instead of listening to l' orbe-ciel:
	dire "De nouveau, la douce mélopée se fait entendre. De nouveau, je ferme les yeux. Et quand je les rouvre…[à la ligne]";
	attendre une touche;
	maintenant le player est le Nâga;
	essayer de looking.

Part 2 - Intro

When play begins:
	dire "Jamais je n'avais nagé aussi vite. J'avais mal et j'aurais aimé m'arrêter, mais je ne pouvais pas.";
	attendre une touche;
	dire "L'Orbe. Il n'y avait que cela qui comptait désormais. L'avenir du monde reposait littéralement entre mes mains.";
	attendre une touche;
	dire "L'Orbe, seule arme capable de vaincre le Démon de la Terre, éveillé après un long sommeil de cent mille ans.";
	attendre une touche;
	dire "Et moi, jeune Nâga que la prophétie annonçait comme le sauveur, devais le vaincre. Mais comment[_]?";
	attendre une touche;
	dire "[à la ligne]J'aperçus ma cité en contrebas[_]: je le saurais bientôt…[saut de paragraphe]";
	maintenant le story viewpoint est la première personne du singulier;
	maintenant le right hand status line est "[turn count]";
	attendre une touche.

Book 2 - Terre

La terre est une région.
Index map with room-colour of la terre set to "Dark Golden Rod".

Part 1 - Grand-place

La grand-place est un endroit dans la terre. "La place pavée sur laquelle nous nous sommes réunis lorsque la terre avait tremblé. Elle est maintenant vide, et ma présence ne semble pas avoir été remarquée. Heureusement, je pense, car je n'aurais pas aimé me voir acclamé par la foule alors que le monde n'est pas encore hors de danger[première fois]. Je devrais aller à la rencontre de la prêtresse[seulement][si l' autel est activé]. [à la ligne][parmi]Il semble y avoir eu une explosion et les dalles ont volé en éclats[_]: une entrée s'est creusée dans le fond de l'océan[ou]Un tunnel béant s'ouvre au centre de la place[stoppant][fin si].[à la ligne]Le temple se dresse au sud.".
Le printed name est "Sur la grand-place".

Instead of going nulle part from la grand-place, dire "Il me reste encore beaucoup à faire avant de pouvoir me promener de nouveau dans ma cité bien-aimée.".

Instead of going from la grand-place to le tunnel when l' autel n'est pas activé, dire "Je ne peux pas aller dans cette direction.".

After going to la grand-place when l' autel est activé:
	dire "Alors que je retourne sur la grand-place, je vois que le sol a volé en éclat, révélant un tunnel qui s'enfonce au plus profond de la Terre.";
	continuer l'action.

Chapter 1 - Temple (objet)

Le temple-décor est une chose décorative dans la grand-place.
Le printed name est "temple".
La description est "Notre temple, où nous vénérons notre Déesse. Maintenant, il me rappelle mes aventures sur cette île où habitaient nos ancêtres.".

Comprendre "temple" comme le temple-décor.

Instead of entering le temple-décor, essayer de going sud.

Part 2 - Temple

Le temple est au sud de la grand-place. "Notre lieu de culte, drapé de pénombre et silencieux, comme toujours. Maintenant que je le sais, je trouve étrange que nous vénérions la Déesse du Ciel, alors qu'on ne le voit jamais depuis le fond de l'océan. La prêtresse le sait-elle[_]?".
Le printed name est "Dans le temple sacré de notre Déesse".
Le temple est à l'intérieur de la grand-place.
Le temple est dans la terre.

Instead of exiting when l' emplacement est le temple, essayer de going nord.

Index map with the temple mapped east from the hall.

Chapter 1 - Prêtresse

La prêtresse est une femme privately-named dans le temple. "La prêtresse semble [si l' orbe-terre n'est pas activé]m'attendre[sinon]froissée[fin si]."
L' indefinite article est "la".
La description est "Prêtresse est une haute fonction chez notre peuple. Elle dirige notre culte et nous empêche de nous éloigner du droit chemin. Notre prêtresse actuelle est avisée et nous guide avec sagesse. Elle saura que faire.".

Comprendre "pretresse" comme la prêtresse.

Instead of giving l' orbe-terre to la prêtresse, essayer de talking to la prêtresse.

Instead of talking to la prêtresse pour la première fois:
	dire "«[_]Je t'attendais, déclare-t-elle en me voyant entrer. As-tu réussi[_]?";
	attendre une touche;
	dire "[à la ligne][--] Oui, répondissé-je en lui tendant l'Orbe. Voici ce que j'ai trouvé sur l'île. Mais que dois-je faire maintenant[_]?[_]»";
	attendre une touche;
	dire "[saut de paragraphe]Elle ne dit rien et se saisit de l'objet. Elle l'examine attentivement, semble réfléchir intensément, et se décide finalement à le poser sur l'autel. Elle murmure alors d'inaudibles prières…";
	attendre une touche;
	dire " mais rien ne se passe[_]; elle me prie de le reprendre, l'air dépitée.";
	attendre une touche;
	dire "[à la ligne]«[_]Je ne sais pas, finit-elle par annoncer. Je pense que c'est au sauveur de la prophétie de découvrir le secret de cet artefact. Je ne peux que te souhaiter bonne chance. Je suis désolée.[_]»";
	attendre une touche;
	dire "[saut de paragraphe]Je connais notre prêtresse. Après un tel échec, elle ne parlera plus. Que puis-je faire, dans ce cas[_]?";
	attendre une touche;
	maintenant l' orbe-terre est activé.
	
Instead of talking to la prêtresse, dire "Après cet échec, elle ne parlera plus.".

Part 3 - Tunnel

Instead of going from le tunnel to la grand-place, dire "Je ne peux pas revenir en arrière, malgré mon appréhension de continuer".

Before going to le tunnel when l' autel est activé pour la première fois:
	dire "Hésitant, je m'avance dans le trou nouvellement apparu. Je nage dans un boyau qui paraît sans fin et qui s'enfonce au plus profond de la terre. Enfin, j'en atteins le bout, une poche d'air. J'émerge.";
	attendre une touche.

Le tunnel est en dessous de la grand-place. "Un sombre couloir au cœur de la terre. À l'une des extrémités, de l'eau, directement reliée jusqu'au fond de l'océan. De l'autre, à l'est, une sinistre lueur rougeoyante m'attend.".
Le printed name est "Le long d'un tunnel".
Le tunnel est dans la terre.

Chapter 1 - Eau

L' eau (f) est une chose décorative dans le tunnel.
La description est "L'eau de l'océan, qui m'a conduit jusqu'ici.".

Comprendre "laver [quelque chose]" comme rubbing.

Instead of rubbing le player dans le tunnel when le player est ensanglanté:
	dire "Je plonge dans l'eau et me lave de tout ce sang démoniaque. Enfin, je ressors.";
	maintenant le player n'est pas ensanglanté.

Chapter 2 - Lueur

La lueur est une chose décorative dans le tunnel.
La description est "Une inquiétante lueur rouge. Elle provient de l'est.".

Part 4 - Entrée

L' entrée (f) est à l'est du tunnel. "[si la lave est solide]La lave s'est solidifiée comme par magie, mais la chaleur reste présente. Je devrais continuer au nord[sinon]À peine fait-on quelques pas que la chaleur se fait ressentir[_]; en effet, cet endroit n'est qu'un mince bras de roche relié à une île minuscule entourée de lave en fusion. Impossible de traverser ce magma brûlant sans pont. Pourtant, je vois une entrée au nord[fin si].".
Le printed name est "Sur une île entourée de lave [si la lave est solide]solidifiée[sinon]en fusion[fin si]".
L'entrée est privately-named.
L' entrée est dans la terre.

After going to l' entrée:
	afficher la citation encadrée "L'Orbe te protège de la chaleur.";
	continuer l'action.

Instead of going nord from l' entrée when la lave n'est pas solide:
	dire "Une petite voix en moi me dit que je n'ai rien à craindre, et j'avance avec détermination vers la lave, la croyant.";
	attendre une touche;
	dire "Pourtant, elle se trompait.";
	attendre une touche;
	fin de l'histoire en disant "Vous êtes mort[_]!".

Chapter 1 - Œil

L' oeil est une chose fixée sur place dans l' entrée. "[si crevé]Une horreur sanglante[sinon]Un énorme œil[fin si] est enchâssé dans la roche au centre de l'île.".
Le printed name est "œil".
La description est "Un [si crevé]reste d[']œil, éclaté et sanglant[sinon]énorme globe oculaire injecté de sang, ne restant jamais immobile plus d'une seconde. Une telle chose ne peut être que démoniaque[fin si].".
L' oeil peut être crevé. L' oeil est non crevé.

Comprendre "crever [quelque chose]" comme attacking.

Instead of attacking l' oeil:
	si le player porte la stalagmite:
		dire "Je lève la stalagmite haut par-dessus ma tête, et l'abat sur l'immonde œil. Il éclate, et le sang gicle sur moi. Je crois distinguer un lointain et grave cri de douleur.[à la ligne]Ne voyant pas l'intérêt de garder la stalagmite, je m'en débarrasse.";
		maintenant le player est ensanglanté;
		maintenant l' oeil est crevé;
		retirer la stalagmite du jeu;
		afficher la citation encadrée "Reviens.";
	sinon:
		dire "Pas avec mes mains nues.".

Chapter 2 - Stalagmite

La stalagmite est une chose dans l' entrée. "Une longue stalagmite esseulée tente d'atteindre le plafond, en vain.".
La description est "[si manipulé]La salagmite que j'ai arrachée à la terre. Elle fait une bonne arme malgré son poids énorme[sinon]Une longue colonne pointue, dressée vers les noires hauteurs, mais ne les atteignant pas[fin si].".

Instead of taking la stalagmite pour la première fois, dire "Rassemblant mes forces, je saisis la stalagmite et tente de la briser. Je relâche bientôt l'effort. Il faudra que je réessaie.".

Instead of attacking la stalagmite non manipulée, essayer de taking la stalagmite.

Chapter 3 - Lave

La lave est une chose décorative dans l' entrée.
La description est "[si solide]La lave s'est solidifiée comme par magie, et me permet maintenant de continuer vers le nord[sinon]La lave en fusion rend toute progression impossible[fin si].".
La lave peut être solide. La lave is not solide.

Part 5 - Campement

Le campement est au nord de l' entrée. "[première fois]Je m'enfonce de plus en plus profondément dans la Terre. La chaleur augmente et la luminosité baisse. [seulement]Me voici dans un étrange campement. Des pieux entourent cet endroit dans un semblant de barricade et des débris témoignent que quelque chose a déjà vécu ici. Mais quoi[_]? Seraient-ils encore là[_]? Et surtout, représenteraient-ils un danger[_]?[à la ligne]Il semblerait que le chemin continue au nord.".
Le printed name est "Au cœur d'un campement abandonné".
Le campement est dans la terre.

Chapter 1 - Débris

Les débris sont des choses décoratives privately-named dans le campement.
La description est "Les gens qui vivaient ici semblent avoir quitté précipitamment les lieux. Étrangement, tout est resté intact[_]; seule la poussière témoigne du temps qui a passé. Qu'est-ce qui a bien pu se passer ici[_]? Cela fait froid dans le dos.".

Comprendre "debris" comme les débris.

Chapter 2 - Coffre

Le coffre est un conteneur ouvrable fermé verrouillé verrouillable dans le campement.
La description est "Ce petit coffre paraissant millénaire m'intrigue. Il a l'air de contenir quelque chose de précieux, pourtant il a été abandonné. Que peut-il bien renfermer[_]?".

Chapter 3 - Note

La note est une chose dans le coffre.
La description est "Un petit morceau de papier jauni, ayant résisté au temps. Malheureusement, son contenu est écrit dans une langue qui m'est inconnue. Cependant, quelque chose a été griffonné sur le verso[_]: un réseau compliqué de lignes et colonnes s'entrelaçant, rappelant le plan d'une ville, ainsi qu'une flamme.".

Instead of reading la note, dire "Je n'arrive pas à comprendre ce langage.".

Carry out examining la note pour la première fois:
	incrémenter le score.

Chapter 4 - Lampe

La lampe est une chose dans le campement.
La description est "Une antique lampe, semblant avoir résisté au temps. Elle contient un dispositif optique compliqué, témoin d'une science avancée. [si le cristal est une partie de la lampe]Le cristal est inséré dans la lampe, qui est allumée[sinon]Elle pourrait m'être utile, si je savais l'allumer[fin si]."

Instead of switching on la lampe when le cristal n'est pas une partie de la lampe, dire "Il n'y a aucun dispositif permettant de l'allumer. Cependant, je peux voir une sorte de socle à l'intérieur.".

Instead of switching on la lampe when le cristal est une partie de la lampe, dire "Elle est déjà allumée.".

Instead of switching off la lampe when le cristal est une partie de la lampe, dire "Je dois retirer le cristal pour l'éteindre.".

Chapter 5 - Sac

Le sac est un conteneur ouvrable fermé non verrouillable dans le campement.
La description est "Une vieille besace tombant presque en morceaux. De même, elle a été abandonnée. Pourquoi donc[_]?".

Comprendre "besace" comme le sac.

Chapter 6 - Cristal

Le cristal est une chose dans le sac.
La description est "Une pierre faiblement luminescente, que ne doit se former qu'ici, dans les profondeurs de la terre[si le cristal est une partie de la lampe]. La lampe amplifie son rayonnement, et elle éclaire désormais beaucoup plus loin[fin si].".

Instead of inserting le cristal into la lampe:
	dire "J'insère la pierre dans la lampe[_]; aussitôt, un jeu de miroirs et de lentilles amplifie la lumière du cristal. La lampe est maintenant allumée.";
	maintenant la lampe est lumineuse;
	maintenant le cristal est une partie de la lampe.

Instead of taking or taking off le cristal when le cristal est une partie de la lampe:
	dire "Je retire le cristal de la lampe. Un petit clic se fait entendre, et la lumière disparaît. La lampe est maintenant éteinte.";
	maintenant la lampe est terne;
	maintenant le player porte le cristal.

Part 6 - Carrière

La carrière est au nord du campement. "Cette cavité ne semble pas naturelle. Elle paraît avoir été creusée à la pioche, sûrement pour récupérer ces cristaux luminescents qui parsèment les parois. L'une d'elles est percée d'un mince boyau à l'ouest, et un passage au nord [si les gravats sont fermés]a été bouché par des gravats[sinon]est maintenant ouvert[fin si].".
Le printed name est "Dans une carrière désaffectée".
La carrière est sombre privately-named.
La carrière est dans la terre.

Rule for printing the description of a dark room when l' emplacement est la carrière:
	dire "L'obscurité est constellée de centaines petites lumières, malheureusement pas assez fortes pour y voir clair." instead.

Rule for printing the announcement of darkness when l' emplacement est la carrière:
	dire "L'obscurité revient, et avec elle les centaines de pâles lumières.".

Chapter 1 - Gravats

Les gravats sont une porte.
Les gravats sont non ouvrable.
Les gravats sont au nord de la carrière et au sud de la ville.
La description est "[si fermé]Des gravats semblent avoir été entassés ici volontairement, comme pour barricader le passage. Je dois trouver un moyen de les enlever[sinon]Les gravats sont maintenant éparpillés par terre, rouvrant un passage scellé depuis des millénaires[fin si].".

Comprendre "gravat" comme les gravats.

Instead of opening les gravats fermés, essayer de attacking les gravats.

Chapter 2 - Pioche

La pioche est une chose dans la carrière.
La description est "Une pioche archaïque en pierre, qui ne semble pas avoir bougé depuis des centaines d'années. Elle ne semble pas solide.".

Instead of attacking les gravats fermés:
	si le player porte la pioche:
		dire "Je donne un vigoureux coup afin de déblayer le passage. Mais ce coup est le dernier pour mon instrument, qui rend l'âme. Il va falloir trouver autre chose.";
		retirer la pioche du jeu;
	sinon:
		dire "Les gravats ne bougent pas d'un pouce.".

Part 7 -  Catacombes

Les catacombes sont à l'ouest de la carrière. "La caverne est à présent artificielle, et le mur comporte de nombreuses niches, contenant apparemment… des ossements. [italique]Ils [romain]devaient mettre leurs morts ici. Je sais que les morts sont innofensifs, mais ce lieu ne me rassure guère.".
Le printed name est "Dans des catacombes".
Les catacombes sont sombres.
Les catacombes sont dans la terre.

Rule for printing the description of a dark room when l' emplacement est la carrière:
	dire "Renforcée par l'obscurité d'encre, une lueur fantômatique me dévisage. Rien de très rassurant…[à la ligne]" instead.

Rule for printing the announcement of darkness when l' emplacement est la carrière:
	dire "L'obscurité revient, et avec elle l'étrange lumière.".

Chapter 1 - Ossements

Les ossements sont des choses décoratives dans les catacombes.
La description est "De nombreuses piles d'os sont entassées dans les cavités aux murs. Il vaut mieux ne pas les profaner.".

Comprendre "os" comme les ossements.

Chapter 2 - Crâne

Le crâne de cristal est une chose privately-named dans les catacombes. "Un crâne est posé sur un piédestal".
La description est "Ce crâne est fait du même cristal que ceux de la carrière. Il a été confectionné avec une précision redoutable et un réalisme aberrant. Il n'a pas dû être posé ici par hasard.".

Comprendre "crane" comme le crâne.

After taking le crâne pour la première fois:
	dire "Comme je me saisis du crâne, une voix résonne dans toute la cavité. Elle semble émaner de la relique[_]:";
	attendre une touche;
	dire "[à la ligne]«[_]Qui ose profaner cet endroit sacré[_]?[_]»";
	attendre une touche;
	dire "[saut de paragraphe]Pétrifié par la peur, je parviens néanmoins à prononcer ces mots[_]:[à la ligne]«[_]Je suis ici par la volonté de la Déesse du Ciel, auprès de qui j'ai reçu la mission de défaire le Démon de la Terre.[_]» Je ne sais pas comment cet esprit va réagir. Ici, on ne doit pas connaître la Déesse.";
	attendre une touche;
	dire "[à la ligne]«[_]Si tel est le cas, tu auras besoin de mon aide. Mon esprit réside depuis fort longtemps dans un sanctuaire de la Déesse. Ce crâne est un lien physique qui me lie encore à ce monde. Prends-le, et viens à ma rencontre.[_]»";
	attendre une touche;
	dire "[saut de paragraphe]La voix s'éteint. Je ne sais pas je peux lui faire confiance [--] comment quelqu'un ayant vécu dans les entrailles de la Terre peut-il vénérer la Déesse[_]? [--], mais je n'ai pas trop le choix. Allons-y.".

Part 8 - Ville

La ville est dans la terre. "Le passage débouche sous un haut dôme rocheux. Comme je balaie l'endroit de ma lampe, je découvre de nombreuses cavernes creusées dans les parois[_]; ce peuple devait être troglodyte. À l'ouest, l'une des grottes attire mon attention[_]: l'entrée est plus large et il demeure des vestiges de décorations. Je devrais explorer.".
Le printed name est "Sous un dôme de pierre".
La ville est sombre.

Chapter 1 - Puits

Le puits est une chose fixée sur place dans la ville. "Il y a un puits au centre.".
La description est "Un étrange trou a été creusé dans le sol, et une sorte de monte-charge permet de descendre. Je me penche pour voir ce qu'il y a au fond, mais je ne distingue rien dans les profondeurs insondables[première fois]. J'y lance un caillou et attends longuement. Rien, aucun son ne se fait entendre[seulement]. Il doit être profond.".
Le puits peut être fragile. Il est fragile.

Instead of entering le puits, try going bas.

Instead of going bas from la ville when le puits est fragile:
	dire "J'entre dans le monte-charge et commence à descendre. La poulie grince affreusement, et laisse croire que tout le mécanisme va se briser d'un moment à un autre.";
	attendre une touche;
	dire "Mais il reste intact[_]; c'est la corde qui rompt. S'ensuit alors une longue chute semblant interminable.";
	attendre une touche;
	dire "Mais tout a une fin. Je vois venir la mienne.";
	attendre une touche;
	fin de l'histoire en disant "Vous êtes mort[_]!".

Before going down from la ville when le puits n'est pas fragile:
	dire "Malgré l'air fragile du monte-charge, je décide de l'utiliser. Je descends, doucement afin de ne rien casser. La descente est longue, très longue. Enfin, une lueur se fait voir. J'arrive.";
	attendre une touche.

Part 9 - Antichambre

L' antichambre (f) est à l'ouest de la ville. "Contrairement à ce que l'on pourrait croire depuis l'extérieur, la cavité a été aménagée et le sol est dallé. D'ailleurs, quelque chose y a été gravé. Je peux continuer à l'est.".
Le printed name est "À l'entrée d'une caverne".
L' antichambre est sombre.
L' antichambre est dans la terre. 

Instead of going dedans dans l' antichambre, essayer de going ouest.

Chapter 1 - Dalles

Les dalles (f) sont des choses décoratives dans l' antichambre.
La description est "Sur les dalles recouvrant le sol, une inscription. Elle indique[_]: «[_][italique]Ce lieu de culte est dédié à notre divinité. Ce lieu de culte possède un jumeau, et tout ce qui sera fait par notre divinité dans ce jumeau le sera aussi dans ce lieu de culte. À jamais.[romain][_]» [si we have examined le tympan]N'ai-je pas déjà vu ce texte quelque part[_]?[sinon]Étrange… Que cela peut-il bien signifier[_]?[fin si]".

Comprendre "dalle" ou "sol" comme les dalles.

Instead of reading les dalles, essayer de examining les dalles.

Chapter 2 - Porte

La porte-terre est une privately-named porte. La porte-terre est à l'ouest de l' antichambre et à l'est du lieu de culte.
Le printed name est "porte".
La description est "Une simple porte de pierre brute. Pourtant, une sorte d'énergie semble la parcourir.".

Comprendre "porte" comme la porte-terre.

Carry out opening la porte-terre:
	maintenant la porte-ciel est ouverte.

Carry out closing la porte-terre:
	maintenant la porte-ciel est fermée.

Part 10 - Lieu de culte

Le lieu de culte est sombre. "Une petite pièce hexagonale, n'ayant rien de particulier. C'est donc ça, leur lieu de culte[_]? Pourtant, je sens qu'il y a quelque chose ici.".
Le printed name est "Un lieu de culte".
Le lieu de culte est dans la terre.

Instead of exiting from le lieu de culte, essayer de going est.

Chapter 1 - Table

La table-terre est un support. La table-terre est dans le lieu de culte.
Le printed name est "table".
La description est "Une sorte de table de pierre trônant au centre de la pièce. Tout comme pour la porte, on ressent une sorte d'énergie la parcourir quand on la touche.".

Comprendre "table" comme la table-terre.

Chapter 2 - Coffre

Le coffre-terre est un conteneur fermé non ouvrable fixé sur place privately-named sur la table-terre.
Le printed name est "coffre".
La description est "Un coffre solide en pierre, au lourd couvercle. De même, une sorte d'énergie semble la parcourir. Le coffre est [si ouvert]ouvert[sinon]fermé[fin si].".

Comprendre "coffre" comme le coffre-terre.

Instead of opening le coffre-terre fermé, dire "Impossible à ouvrir.".
Instead of opening le coffre-terre ouvert, dire "Il est déjà ouvert.".

Carry out inserting la lampe into le coffre-terre pour la première fois:
	maintenant le mur est fondu;
	maintenant la boussole est dans le temple-soi.

Carry out inserting la lampe into le coffre-terre:
	maintenant la lumière est dans le coffre-ciel.

Carry out taking la lampe:
	retirer la lumière du jeu.

Part 11 - Mâchoires

Les mâchoires sont dans la terre. "Il fait ici une chaleur torride. Cet endroit est n'est que lave et roc. Je dois être au centre de la Terre. Je ne peux plus remonter[_]; le seul chemin est d'énormes mâchoires aux dents acérées[si les dents sont fermées], mais hermétiquement closes[fin si]. Le Démon de la Terre."
Le printed name est "Devant d'énormes mâchoires".
Les mâchoires sont privately-named.
Les mâchoires sont en dessous de la ville. Up from les mâchoires is nowhere.

Chapter 1 - Dents

Les dents sont une porte non ouvrable. Les dents sont à l'intérieur des mâchoires et à l'extérieur de la gueule.
Le printed name est "mâchoires".
La description est "[si fermées]D'énormes dents grises, acérées, me barrent la route[sinon]Les mâchoires sont ouvertes[fin si]. J'imagine que je devrais pénétrer dans le corps du monstre.".

Comprendre "dent", "machoire" ou "machoires" comme les dents.

Instead of attacking les dents fermées:
	Si le player ne porte pas l' épée:
		dire "Je ne parviendrai à rien avec mes mains minuscules.";
	sinon:
		dire "Confiant, la Lame céleste à la main, je frappe. Un éclair lumineux, un cri de douleur innommable, les mâchoires sont maintenant ouvertes. Je dois maintenant pénétrer dans la bête…[à la ligne]";
		maintenant les dents sont ouvertes;

Part 12 - Démon

Le démon est une région privately-named. Le démon est dans la terre.
Index map with room-colour of démon set to "Dark Red".

L' émergement est une scène.
L' émergement begins when le player est dans la gueule.
L' émergement ends sadly when the time since l' émergement began is 15 minutes. L' émergement ends happily when le coeur est off-stage.

When l' émergement begins:
	dire "Une profonde voix grave emplie de haine résonne alors[_]: «[_]Je t'attendais.[_]»";
	attendre une touche;
	dire "[à la ligne]Je frissonne malgré moi, et resserre ma prise sur la Lame.";
	attendre une touche;
	dire "«[_]Tu ne peux rien contre moi. Tu seras le premier à assister à la dislocation du monde.[_]»";
	attendre une touche;
	dire "[à la ligne]La voix s'éteint, mais résonne encore dans mon esprit. Soudain, le sol tremble, et je sens le Démon monter.";
	attendre une touche;
	dire "[à la ligne]Je dois le vaincre avant qu'il n'atteigne la surface de la Terre[_]!".

Every turn during l' émergement:
	dire "[parmi]Une secousse se fait ressentir[ou]Le Démon de la Terre continue son ascension mortelle[ou]La surface se rapproche [totalement au hasard][_]!".

When émergement ends happily:
	dire "Je brandis la Lame et frappe la pierre de toutes mes forces[_]; elle vole en éclats. Le Démon est secoué de toute part alors qu'un flash lumineux m'aveugle.";
	attendre une touche;
	dire "Toute la réalité s'effondre autour de moi. Serais-je en train de mourir[_]?";
	attendre une touche;
	dire "[à la ligne]Quand je reprends mes esprits, je me trouve devant un château de cristal d'une beauté éblouissante. La Déesse m'attend.";
	attendre une touche;
	dire "[italique]Tu as réussi. Je te remercie d'avoir vaincu le Démon de la Terre. Mais ce n'est pas fini[_]: il te reste une mission à accomplir. Reviens à toi, et je te montrerais la voie…[romain]";
	attendre une touche;
	fin définitive de l'histoire en disant "À suivre…".

When émergement ends sadly:
	dire "Il y a soudain une énorme secousse, ainsi qu'un bruit de tonnerre. Il a émergé. Tout est fini. Il détruira bientôt le monde.";
	attendre une touche;
	fin de l'histoire en disant "Vous avez perdu[_]!".

Chapter 1 - Gueule

La gueule est dans le démon. "Rien n’est organique, tout semble à de la roche sombre. L'air ici est vicié, il pique les yeux et irrite la gorge, mais je ne peux pas rebrousser le chemin. Je dois trouver le moyen de vaincre le Démon. Le passage continue au sud.".
Le printed name est "Dans la gueule du Démon".
La gueule est sombre.

Instead of exiting dans gueule during l' émergement, dire "La gueule s'est refermée, il n'y a plus moyen de sortir. Avançons.".

Chapter 2 - Conduit

Le conduit est au sud de la gueule. "Je suis maintenant dans une sorte de conduit. Le sol est recouvert d'une sorte de liquide rouge luminescent et visqueux. Je peux continuer au sud-ouest, au sud et à l'est.".
Le printed name est "Le long d'un conduit".
Le conduit est sombre.
Le conduit est dans le démon. 

Section 1 - Liquide

Le liquide est une toile de fond dans le conduit et dans la glande.
La description est "Un liquide visqueux et dégoûtant. N'y touchons pas.".

Chapter 3 - Glande

La glande est au sud-ouest du conduit. "Une sorte de cavité où le liquide s'amasse pour coaguler en une sorte de pierre rouge sombre et luisante. Qu'est-ce donc[_]?".
Le printed name est "Dans une petite cavité".
La glande est sombre.
La glande est dans le démon. 

Section 1 - Coeur

Le coeur (f) est une chose fixée sur place dans la glande.
Le printed name est "pierre".
La description est "Une étrange pierre rouge, dégageant une aura maléfique.".

Comprendre "pierre" comme le coeur.

Instead of attacking le coeur pour la première fois:
	dire "Je brandis la Lame et frappe la pierre de toutes mes forces. Le Démon a un sursaut qui fait trembler tout son corps, et la pierre s'enfonce dans ses tissus pour disparaître.";
	let destination be a random room in démon;
	maintenant le coeur est dans la destination

Instead of attacking le coeur for the second time:
	dire "Je brandis la Lame et frappe la pierre de toutes mes forces. Le Démon a un sursaut qui fait trembler tout son corps, et la pierre s'enfonce dans ses tissus pour disparaître.";
	let destination be a random room in démon;
	maintenant le coeur est dans la destination.

Instead of attacking le coeur for the third time:
	retirer le coeur du jeu.

Chapter 4 - Estomac

L' estomac est au sud du conduit. "Cet endroit est rempli de lave. Je ne peux pas continuer.".
Le printed name est "Dans ses entrailles".
L' estomac est dans le démon.

Chapter 5 - Poumon

Le poumon est à l'ouest du conduit. "Ici, les parois sont parsemées de trous, et chacun d'eux renvoie de l'air chaud et fétide. Je dois me trouver dans le système respiratoire.".
Le printed name est "À l'intérieur du poumon".
Le poumon est sombre.
Le poumon est dans le démon.

Book 3 - Ciel

Le ciel est une région.
Index map with room-colour of ciel set to "Sky Blue".

Le nuage est une région. Le nuage est dans le ciel.

Instead of going nulle part from le nuage:
	si le player n'est pas brumeux, dire "La plaine blanche s'étend jusqu'à l'horizon, rase. Inutile de continuer dans cette direction.";
	sinon essayer de looking.

To say Brume:
	dire "Dans le brouillard".

To say brume:
	dire "Un brouillard épais s'est soudainement levé, obstruant la vue à moins d'un mètre. Que peut bien être la raison d'un tel changement climatique[_]? Je n'ai maintenant plus aucuns repères[si l'endroit en dedans de l' emplacement est un endroit] mais il semblerait que je puisse entrer[fin si]".

Part 1 - Hall

Le hall est un endroit dans le ciel. "Une vaste pièce étrange[si l' esplanade n'est pas visitée], sans issues apparentes,[fin si] dont les murs semblent construits de lumière et solides comme le vent. Le sol dallé est transparent, mais je n'arrive pas à distinguer sur quoi est bâti l'édifice, le cristal étant dépoli. Je n'ai jamais vu un endroit aussi beau.".
Le printed name est "Dans [parmi]un[ou]le[stoppant] hall lumineux".

Instead of going haut from le hall when la stèle n'est pas sonnée, dire "Je ne peux pas aller dans cette direction.".
Instead of going dehors when l' esplanade n'est pas visitée, dire "Je ne peux pas aller dans cette direction.".

Index map with the hall mapped east from temple-se.

Chapter 1 - Murs

Les murs sont des choses décoratives dans le hall.
La description est "Des murs de la matière la plus pure, lumineuse. On aurait dit qu'ils ont été construit à partir de rayons.".

Chapter 2 - Sol

Le sol est une chose décorative dans le hall.
La description est "Des dalles de cristal ornent le sol. Elles sont sobres mais magnifiques.".

Comprendre "dalle", "dalles" ou "cristal" comme le sol.

Chapter 3 - Stèle

La stèle est une chose fixée sur place privately-named dans le hall. "Une stèle trône au centre.".
La description est "Un monument imposant, également de cristal, et portant des inscriptions gravées. Je ne sais pas de quelle langue il s'agit et je ne comprends pas ces caractères courbes et entrelacés, mais ils ont quelque chose de familier…[à la ligne]".
La stèle peut être sonnée. La stèle est non sonnée.

Comprendre "stele", "caracteres", "caractere", "inscription", "inscriptions" ou "cristal" comme la stèle.

Instead of reading la stèle:
	dire "Les lettres semblent se mouvoir devant mes yeux et on aurait dit qu'elles essayaient de me transmettre un message… Non, mon imagination me joue des tours.";
	afficher la citation encadrée "Sonne.".

Instead of attacking la stèle pour la première fois:
	maintenant la stèle est sonnée;
	maintenant l' escalier est dans le hall;
	dire "Je frappe la stèle, et un son pur s'en échappe pour résonner dans toute la pièce. Un vent venu de nulle part se lève, et un escalier formé de rayons apparaît. Je ne sais pas où cela peut me mener, mais je n'ai pas trop le choix.".

Instead of attacking la stèle, dire "Elle laisse échapper un son pur.".

Chapter 4 - Escalier

L' escalier est une chose décorative.
La description est "Un étrange escalier fait de rayons lumineux. Pourtant, il ne m'inspire pas de crainte.".

Part 2 - Balcon

La terrasse est au-dessus du hall. "[si le player est brumeux][brume][sinon]Après avoir gravi l'escalier, je débouche à l'air libre. Je suis sur une terrasse, au sommet du bâtiment, une tour. D'ici, on peut admirer le paysage à des lieues à la ronde[_]: une plaine blanche, brillante, immaculée, s'étendant à perte de vue. Çà et là, on peut distinguer des bâtisses, semblables à des mirages. Cet endroit est beau[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Sur une large terrasse[fin si]".
La terrasse est dans le ciel.

Chapter 1 - Déesse

La déesse est une femme privately-named dans la terrasse. "Une femme dotée d'une aura me domine de toute sa hauteur.".
Le printed name est "Déesse du Ciel".
L' indefinite article est "la".
La description est "Une personne plus haute que la normale à la peau dénuée d'écailles et à l'abondante chevelure blonde me regarde fixement de ses yeux bleus comme des saphirs. Elle porte une ample robe blanche et est nimbée d'une aura lumineuse. Une étonnante sérénité émane d'elle.".

Comprendre "femme", "dame", "deesse" ou "deesse du ciel" comme la déesse.

Instead of talking to la déesse pour la première fois:
	dire "[italic type]Enfin.";
	attendre une touche;
	dire "[roman type]De nouveau, la voix cristalline résonne dans ma tête et y reste un moment avant de se dissiper. Je demeure muet, avant de finalement me reprendre[_]:";
	attendre une touche;
	dire "[saut de paragraphe]«[_]Où suis-je[_]? Et qui êtes-vous[_]?[_]»";
	attendre une touche;
	dire "[à la ligne][italic type]Tu ne comprends toujours pas[_]? Je suis la Déesse du Ciel et ce que tu contemples est mon royaume. C'est moi qui t'ai fait venir ici.";
	attendre une touche;
	dire "[à la ligne][roman type]Elle n'a pas remué les lèvres. La voix que j'entends est douce, mais une mystérieuse force s'en dégage. Je réponds[_]:";
	attendre une touche;
	dire "[saut de paragraphe]«[_]Dans ce cas, qu'attendez-vous de moi[_]?[_]»";
	attendre une touche;
	dire "[à la ligne][italic type]De l'aide. Je suis impuissante face au Démon.";
	attendre une touche;
	dire "[à la ligne][roman type]La phrase sonne toujours aussi limpidement en moi, mais son sens met du temps à se frayer dans mon esprit. La Déesse, impuissante[_]?";
	attendre une touche;
	dire "[à la ligne]«[_]Si vous n'êtes pas capable de le vaincre, comment ferais-je, moi[_]? Je ne suis pas un héros, encore moins un dieu[_]![_]»";
	attendre une touche;
	dire "[à la ligne][italic type]Je vainquis jadis le Démon de la Terre, qui sombra alors dans un sommeil de cent mille ans, au cours desquels il a pansé ses plaies. Pire, il s'est renforcé. Maintenant, je ne peux plus avoir d'influence sur le monde d'en bas, et seul quelqu'un d'extérieur à notre conflit peut agir.";
	attendre une touche;
	dire "Toi.";
	attendre une touche;
	dire "[roman type]«[_]Très bien, il s'agit de la prophétie, après tout, me résigné-je Que dois-je faire[_]?[_]»";
	attendre une touche;
	dire "[à la ligne][italic type]Trouve les sanctuaires qui parsèment mon royaume. Chacun d'eux renferme un pouvoir immense que tu pourras utiliser. Va maintenant.";
	attendre une touche;
	dire "[à la ligne][roman type]«[_]Attendez…[_]» Trop tard[_]: il y a soudain un éclair aveuglant, et je me retrouve sur la plaine blanche. Bon, tâchons de trouver l'un de ces sanctuaires.";
	maintenant l' initial appearance de la déesse est "La Déesse du Ciel me domine de toute sa hauteur.";
	attendre une touche;
	maintenant le player est dans l' esplanade.

Instead of talking to la déesse, dire "[italic type]Vite, le temps nous est compté.[roman type][à la ligne]".

Part 3 - Esplanade

L' esplanade (f) est à l'extérieur du hall. "[si le player est brumeux][brume][sinon]Je suis en extérieur. Le soleil brille fort, mais il n'est pas aveuglant. Tout autour de moi, la plaine blanche, semblable à un immense nuage, dans toutes les directions. Au centre de l'esplanade, une tour se dresse vers le ciel - celle où j'ai rencontré la Déesse.[à la ligne]Quatre bâtiments se dressent sur l'horizon[_]: au nord-ouest, au nord-est, au sud-est et au sud-ouest[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Sur une large esplanade[fin si]".
L' esplanade est dans le nuage. 

Chapter 1 - Autel

L' autel est un support. L' autel est dans l' esplanade. "Il y a un petit autel près de moi.".
La description est "Un petit monument sobre et sans ornements. Une si petite chose pourrait-elle être un sanctuaire[si non activé][_]? Et que pourrais-je faire pour l'utiliser[fin si][_]?".
L' autel peut être activé. L' autel est non activé.

Instead of putting l' orbe-ciel on l' autel non activé:
	dire "Me rappelant de ce qu'avait fait la prêtresse, je pose l'Orbe sur l'autel. Ce dernier se met à luire, puis à briller.  Puis tout redevient comme avant. Tout en récupérant l'orbe, je me demande ce qui a changé.";
	afficher la citation encadrée "Retourne sur terre.";
	maintenant l' autel est activé.

Instead of going from l' esplanade when l' oeil n'est pas crevé:
	dire "Je m'apprête à aller plus loin, quand soudain, un mur de roche jaillit du sol et s'élève, me barrant la route. Je recule, et l'obstacle de roche disparaît, aussi vite qu'il était apparu.".

Part 4 - Plaines

Chapter 1 - Tornade

La tornade est une chose fixée sur place. "Une effrayante tornade se dresse proche de moi.".
La description est "Un tourbillon de poussière étincelante, qui semble composée d'énergie pure. Mieux vaut s'en éloigner.".

When play begins:
	maintenant la tornade est dans a random room in nuage.

Every turn:
	si la tornade n'est pas dans l' emplacement:
		let the way be a random direction;
		si the room way from the location of la tornade is in nuage:
			maintenant la tornade est dans the room way from the location of la tornade;
			si le player can see la tornade, dire "Une tornade étincelante apparaît.";
		si a random chance of 1 in 10 succeeds and l' esprit is in nuage, maintenant the tornade is in the location of l' esprit.

Instead of taking la tornade, dire "Impossible.".

Instead of entering la tornade:
	dire "Une idée folle a jailli dans mon esprit, et je me dirige maintenant dans la tornade et pénètre à l'intérieur. Je me sens emporté…[à la ligne]";
	attendre une touche;
	si une chance de 1 sur 10 réussit:
		maintenant le player est dans la chambre;
	sinon:
		maintenant le player est dans a random room in nuage.

Section 1 - Trouver la tornade - not for release

Tornading is an action out of world applying to nothing.
Understand "tornade" as tornading.

Report tornading:
	dire "[the best route from the location to the location of tornade].".

Chapter 2 - Plaine nord

La plaine-n est au nord de l' esplanade. "[si le player est brumeux][brume][sinon]Elle se déroule dans toute les directions, à l'infini. Seuls points de repère, les bâtiments se situant à l'ouest [--] le plus proche [--], au nord-est, au sud-ouest et au sud-est. La tour est au sud[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-n est privately-named.
La plaine-n est dans le nuage.

Chapter 3 - Plaine nord-est

La plaine-ne est au nord-est de l' esplanade et à l'est de la plaine-n. "[si le player est brumeux][brume][sinon]Toujours ce même sol cotonneux et immaculé. Deux bâtiments sont près d'ici, au nord-ouest et au sud. Les deux autres semblent se fondre au ciel à l'ouest et au sud-ouest. La tour est au sud-ouest[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-ne est privately-named.
La plaine-ne est dans le nuage.

Chapter 4 - Plaine est

La plaine-e est à l'est de l' esplanade, au sud de la plaine-ne et au sud-est de la plaine-n.  "[si le player est brumeux][brume][sinon]La plaine blanche, semblable à un nuage. À moins que ce soit vraiment un nuage. Comment fais-je pour tenir dessus[_]? Il y a une sorte de temple au sud-est. D'autres bâtiments semblent posés sur l'horizon au nord, au nord-ouest et au sud-ouest. La tour est à l'ouest[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-e est privately-named.
La plaine-e est dans le nuage.

Chapter 5 - Plaine sud-est

La plaine-se est au sud-est de l' esplanade et au sud de la plaine-e. "[si le player est brumeux][brume][sinon]Le soleil est éclatant, mais sa douce chaleur est apaisante et son reflet sur le sol immaculé n'est pas aveuglant. Un temple se situe à l'est. Sinon, il y en a un proche vers l'ouest. Les autres sont au nord-ouest et au nord-est. La tour, quant à elle, est au nord-ouest[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-se est privately-named.
La plaine-se est dans le nuage.

Chapter 6 - Plaine sud

La plaine-s est au sud de l' esplanade, au sud-ouest de la plaine-e et à l'ouest de la plaine-se. "[si le player est brumeux][brume][sinon]Le ciel est d'un bleu azur, si profond que l'on finit par s'y perdre[_]; si on ne se perd pas d'abord dans la plaine[_]! La tour est au nord, et un bâtiment a été construit au sud-ouest. Sinon, on peut voir un monument proche à l'est et dans le lointain deux autres au nord-ouest et au nord-est[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-s est privately-named.
La plaine-s est dans le nuage.

Chapter 7 - Plaine sud-ouest

La plaine-so est au sud-ouest de l' esplanade et à l'ouest de la plaine-s. "[si le player est brumeux][brume][sinon]Rien de nouveau. La tour de lumière est au nord-est et un monument se dresse au sud. Deux autres bâtiments se dressent plus loin, l'un au nord et l'autre à l'est. On distingue à peine celui au nord-est[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans plaine[fin si]".
La plaine-so est privately-named.
La plaine-so est dans le nuage.

Chapter 8 - Plaine ouest

La plaine-o est à l'ouest de l' esplanade, au nord-ouest de la plaine-s et au nord de la plaine-so. "[si le player est brumeux][brume][sinon]La tour où j'ai rencontrée la Déesse s'élève à l'est, des bâtiments se dressent au sud et au nord-ouest et je distingue des édifices loin au nord-est et plus à l'est. Hormis cela, la plaine, toujours la plaine[fin si]."
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-o est privately-named.
La plaine-o est dans le nuage.

Chapter 9 - Plaine nord-ouest

La plaine-no est au nord-ouest de l' esplanade, au nord de la plaine-o et à l'ouest de la plaine-n. "[si le player est brumeux][brume][sinon]Je suis proche de la tour, au sud-est, et d'une sorte de temple au nord-ouest. Par-delà la plaine immaculée, je peux voir deux autres bâtiments, au nord-est et au sud[fin si]."
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-no est privately-named.
La plaine-no est dans le nuage.

Chapter 10 - Plaine nord-ouest nord

La plaine-non est au nord de la plaine-no et au nord-ouest de la plaine-n. "[si le player est brumeux][brume][sinon]Je m'approche du temple, qui se situe à l'ouest. Autrement, je me retourner sur mes pas où vers les autres bâtiments au sud et au sud-est[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-non est privately-named.
La plaine-non est dans le nuage.

Chapter 11 - Plaine nord-ouest ouest

La plaine-noo est au sud-ouest de la plaine-non, à l'ouest de la plaine-no et au nord-ouest de la plaine-o. "[si le player est brumeux][brume][sinon]Je me fatigue un peu du paysage monotone, mais on ne se lasse pas facilement de son étrange beauté. Des temples au nord, au sud, une tour de lumière au sud-est et, chose qui m'intrigue et m'attire, de hauts piliers au sud-ouest. Je devrais m'approcher[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-noo est privately-named.
La plaine-noo est dans le nuage.

Chapter 12 - Plaine ouest ouest

La plaine-oo est au sud de la plaine-noo, au sud-ouest de la plaine-no, à l'ouest de la plaine-o et au nord-ouest de la plaine-so. "[si le player est brumeux][brume][sinon]Je m'éloigne de l'esplanade, et me situe maintenant entre deux monuments au nord et au sud. Mais le plus intéressant se trouve à l'ouest[_]: de singulières colonnes s'élèvent vers le ciel [--] même si j'y suis déjà[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-oo est privately-named.
La plaine-oo est dans le nuage.

Chapter 13 - Plaine sud-ouest ouest

La plaine-soo est au sud de la plaine-oo, au sud-ouest de la plaine-o et à l'ouest de la plaine-so. "[si le player est brumeux][brume][sinon]Je me situe maintenant entre deux constructions[_]: le temple au sud-est et d'étranges colonnes dans la direction opposée. La plaine est vaste, et on finit par reconnaître le paysage[_]: la tour et un autre édifice à l'est, un monument au nord… Tâchons de ne pas se perdre[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-soo est privately-named.
La plaine-soo est dans le nuage.

Chapter 14 - Plaine est est

La plaine-ee est au sud-est de la plaine-ne, à l'est de la plaine-e et au nord-est de la plaine-se. "[si le player est brumeux][brume][sinon]Je me situe plein est de la tour lumineuse et au nord d'une construction semblable à un temple. D'ailleurs, il en a d'autres, au nord, au nord-ouest et au nord-est[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-ee est privately-named.
La plaine-ee est dans le nuage.

Chapter 15 - Plaine nord-est est

La plaine-nee est à l'est de la plaine-ne, au nord-est de la plaine-e et au nord de la plaine-ee. "[si le player est brumeux][brume][sinon]Je ne me trouve près d'aucun bâtiment, mais je peux voir un édifice au nord et au sud, ainsi que la tour à l'ouest. Je devrais m'approcher de l'un d'eux[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-nee est privately-named.
La plaine-nee est dans le nuage.

Chapter 16 - Plaine nord-est nord-est

La plaine-nene est au nord-est de la plaine-ne et au nord de la plaine-nee. "[si le player est brumeux][brume][sinon]Il y a un temple au nord-est, et la tour est au sud-ouest. Sinon, tout se perd et se confond dans l'immensité de la plaine blanche et la légère brume étincelante[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-nene est privately-named.
La plaine-nene est dans le nuage.

Chapter 17 - Plaine nord-est nord-est nord

La plaine-nenen est au nord de la plaine-nene. "[si le player est brumeux][brume][sinon]J'ai fait un bon bout de chemin, et me voilà maintenant à l'ouest d'un temple. Autrement, j'ai perdu mes points de repère qui se sont perdus dans la brume. Je devrais peut-être revenir[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-nenen est privately-named.
La plaine-nenen est dans le nuage.

Chapter 18 - Plaine nord-est nord-est est

La plaine-nenee est au sud-est de la plaine-nenen, à l'est de la plaine-nene et au nord-est de la plaine-nee. "[si le player est brumeux][brume][sinon]Je suis au sud du temple le plus proche, et loin de tout le reste, que l'on peut à peine distinguer sur l'horizon. Cette plaine est immense, je vais finir par me perdre si je ne fais pas attention[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Dans la plaine[fin si]".
La plaine-nenee est privately-named.
La plaine-nenee est dans le nuage.

Chapter 19 - Plaine sud sud

La plaine-ss est au sud-est de la plaine-so, au sud de la plaine-s et au sud-ouest de la plaine-se. "[si le player est brumeux][brume][sinon]Ici, la plaine s'arrête brusquement et un précipice d'un bleu insondable me sépare d'un autre nuage. Sur celui-ci a été construit un palais immense, semblable à une vision tellement il est beau. Mais ne nous attardons pas, il y a un temple à l'ouest et un autre à l'est. Les autres édifices sont loin au nord[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Au bout de la plaine[fin si]".
La plaine-ss est privately-named.
La plaine-ss est dans le nuage.

Chapter 20 - Plaine sud-est sud

La plaine-ses est à l'est de la plaine-ss, au sud-est de la plaine-s et au sud de la plaine-se. "[si le player est brumeux][brume][sinon]Le nuage sur lequel je me trouve s'arrête là. De l'autre côté, sur un minuscule îlot, est dressé le plus beau bâtiment que j'ai jamais vu[_]: les murs semblent de lumière et les toits de vent. Malheureusement, je dois me retourner[_]: il y a un édifice au nord-est, un autre un peu plus loin à l'ouest. La tour est au nord-ouest et il y a un autre bâtiment à l'horizon au nord[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Au bout de la plaine[fin si]".
La plaine-ses est privately-named.
La plaine-ses est dans le nuage.

Part 5 - Temple nord-ouest

Le temple-no est au nord de la plaine-noo, au nord-ouest de la plaine-no et à l'ouest de la plaine-non. "[si le player est brumeux][brume][sinon]Je me trouve maintenant devant un temple fait de cristal semblant capter la lumière afin de la restituer, encore plus brillante. Une énorme colonnade translucide accueille les visiteurs. Ce serait quasi-criminel de ne pas entrer[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Devant un temple de cristal[fin si]".
Le temple-no est dans le nuage.

Chapter 1 - Intérieur du temple

Le temple-noi est à l'intérieur du temple-no. "Une grande pièce illuminée par le plafond transparent concentrant la lumière. Un silence absolu règne ici, hormis le son cristallin que produisent mes pas. Il doit s'agir d'un sanctuaire.".
Le printed name est "À l'intérieur du temple".
Le temple-noi est dans le ciel.

Index map with temple-noi mapped west from the temple-no.

Section 1 - Plaque

La plaque est une chose fixée sur place dans le temple-noi. "Une plaque est accrochée au mur.".
La description est "Une plaque de marbre a été fixée au mur. Une inscription y a été gravée.".

Instead of reading la plaque, dire "«[_]Toi qui viens en ces lieux, agenouille-toi et recueille-toi. Tes vœux seront alors exaucés.[_]»[à la ligne]".

Instead of praying dans le temple-noi pour la première fois:
	dire "Comme l'indique l'inscription, je me mets à prier. Au même moment, l'Orbe se met à vibrer, plus lumineux et plus chaud. Puis tout s'arrête. Je devrais retourner sous terre, voir se qui s'est passé.";
	maintenant la lave est solide.

Part 6 - Temple nord-est

Le temple-ne est à l'est de la plaine-nenen, au nord-est de la plaine-nene et au nord de la plaine-nenee. "[si le player est brumeux][brume][sinon]Une merveille d'architecture se dresse devant moi. Semblant faite de cristal, la construction paraît d'une fragilité extrême. C'est pourtant ce qui la rend belle. Malheureusement, il n'y a pas d'entrée, et je ne pourrai pas visiter cet étrange monument[fin si].".
Le printed name est "[si le player est brumeux][Brume][sinon]Devant un temple de cristal[fin si]".
Le temple-ne est dans le nuage.

Instead of going dedans from le temple-ne, dire "Il n'y a pas d'entrée visible, impossible de pénétrer dans le bâtiment.".

Chapter 1 - Intérieur du temple

Le temple-nei est un endroit. "[première fois]Étrangement, je me retrouve dans ce temple, alors que je n'y étais pas auparavent. Cela doit être à cause du crâne. [seulement]Voci donc le sanctuaire où résiderait un spectre mystérieux. Cet endroit est lumineux, accueillant, et ne ressemble pas à la demeure d'un fantôme. Et pourtant, je sens une présence, même si je ne vois rien. L'esprit daignera-t-il se montrer[première fois] ou devrais-je trouver moi-même la solution[seulement][_]?".
Le printed name est "À l'intérieur du temple".
Le temple-nei est dans le ciel.

Section 1 - Canne

La canne est une chose.
Le printed name est "canne d'argent".
La description est "Un magnifique objet en argent ciselé. Il est d'une perfection extrême, normalement impossible à atteindre. Il appartient à la Déesse, après tout.".

Instead of praying dans le temple-nei pour la première fois:
	dire "Comme la dernière fois, je m'agenouille et commence ma prière. Un tourbillon de vent se lève au centre de la pièce. Au centre de celui-ci, un objet apparaît.";
	attendre une touche;
	dire "[italic type]Voici la canne de la Déesse. Elle contient une quantité phénoménale d'énergie, mais inutilisable tel quel.";
	attendre une touche;
	dire "Cherche la tornade qui court sur les plaines. Elle la libérera.";
	dire romain;
	maintenant la canne est dans le temple-nei.

Instead of throwing la canne at la tornade:
	dire "Je me saisis fermement de la canne et la lance de toutes mes forces dans la tornade. Elle atteint son cœur, et explose dans une poussière d'étoile scintillante. Maintenant que son énergie est libérée, voyons que faire.";
	retirer la canne du jeu;
	maintenant les gravats sont ouverts. 

Section 2 - Clef rouillée

La clef rouillée est une chose privately-named dans le temple-nei.
La description est "Une petite clef rouillée, semblant sur le point de se désagréger. Elle ne semble pas provenir de l'endroit où je l'ai trouvée.".
La clef rouillée ouvre le coffre. 

Comprendre "cle/clef rouillee" ou "cle" comme la clef rouillée.

Part 7 - Temple sud-ouest

Le temple-so est au sud-est de la plaine-soo, au sud de la plaine-so, au sud-ouest de la plaine-s et à l'ouest de la plaine-ss. "[si le player est brumeux][brume][sinon]Une magnifique construction, et le mot est faible. Fait d'un cristal d'une pureté impossible à atteindre, elle rend bien pâle notre architecture, dont nous sommes pourtant fiers. Un magnifique tympan surplombe l'entrée[fin si]."
Le printed name est "[si le player est brumeux][Brume][sinon]Devant un temple de cristal[fin si]".
Le temple-so est dans le nuage.

Section 1 - Tympan

Le tympan est une chose décorative dans le temple-so.
La description est "Le tympan de cristal a été soigneusement sculpté, et on peut y lire[_]: «[_][italique]Ce temple a été construit pour éclairer le chemin d'un peuple, jusqu'alors dans l'ombre. Ce temple possède un jumeau, et tout ce qui sera fait dans ce jumeau le sera aussi dans ce temple. À jamais.[romain][_]» [si we have examined les dalles]N'est pas déjà vu ce texte quelque part[_]?[sinon]Je devrais chercher ce jumeau.[fin si]".

Instead of reading le tympan, essayer de examining le tympan.

Chapter 1 - Intérieur du temple

Le temple-soi is un endroit dans le ciel. "Un autre temple de cristal, toujours aussi beau. On ne peut s'en lasser. Devrais-je prier[_]?".
Le printed name est "À l'intérieur du temple".

Instead of praying dans le temple-soi pour la première fois, dire "De nouveau, je me mets à prier. Mais rien ne se passe.".

Section 1 - Porte

La porte-ciel est une non ouvrable privately-named porte. La porte-ciel est à l'intérieur du temple-so et à l'extérieur du temple-soi.
Le printed name est "porte".
La description est "Une lourde et belle porte de marbre, sobre et inébranlable. Curieusement, elle n'a pas de poignée ni de serrure.[si fermée] Comment l'ouvrir[_]?[sinon]Elle est maintenant ouverte. Étrange…[à la ligne][fin si]".

Comprendre "porte" comme la porte-ciel.

Instead of opening la porte-ciel fermée, dire "Sans poignée ni serrure[_]? Impossible.".

Section 2 - Table

La table-ciel est un support. La table-ciel est dans le temple-soi.
Le printed name est "table".
La description est "Une table de marbre finement sculptée trône au centre de la pièce. C'est une véritable œuvre d'art.".

Comprendre "table" comme la table-ciel.

Section 3 - Coffre

Le coffre-ciel est un conteneur fermée transparent ouvrable fixé sur place privately-named sur la table-ciel.
Le printed name est "coffre".
La description est "Une sorte de coffre transparent, semblant capter la lumière et la concentrer vers un point précis du mur. [si la lampe est dans le coffre-terre]Le puissant rayon, jaillissant de nulle part, y a créé un trou[sinon]Malheureusement, le rayon n'est pas assez puissant et rien ne se passe[fin si]. Le coffre est [si ouvert]ouvert[sinon]fermé[fin si].".

Comprendre "coffre" comme le coffre-ciel.

Carry out opening le coffre-ciel:
	maintenant le coffre-terre est ouvert.

Carry out closing le coffre-ciel:
	maintenant le coffre-terre est fermé.

Section 4 - Mur

Le mur est une chose décorative dans le temple-soi.
La description est "Le mur est entièrement en cristal[si fondu]. Il a été percé par la lumière[fin si].".
Le mur peut être fondu.

Section 5 - Lumière

La lumière est une chose décorative privately-named.
L' indefinite article est "de la".
La description est "[si la lampe est dans le coffre-terre]Un puissant rayon de lumière, jaillissant de nulle part, se dirige vers le mur. Il a fait fondre une partie de ce dernier[sinon]Un minuscule faisceau se réduisant à un point au mur. Il n'est pas assez puissant et rien ne se passe[fin si].".

Comprendre "lumiere", "rayon" ou "faisceau" comme la lumière.

Section 6 - Boussole

La boussole est une chose.
La description est "Un étrange dispositif contenant une aiguille en pierre bleue. Celle-ci [si l' emplacement n'est pas dans le nuage]pointe vers la sortie[sinon si l' emplacement est la cache]tourne sur elle-même à grande vitesse, affolée[sinon]indique une direction[_]: [le best route from l' emplacement to la cache][fin si]. Sur le revers, il est marqué[_]: «[_][italique]Prie[romain][_]»."

La cache is a room that varies.
When play begins:
	maintenant la cache is a random room in nuage.

Instead of praying dans la cache pour la première fois:
	maintenant le puits n'est pas fragile;
	dire "Je me mets à prier, à l'endroit indiqué par la boussole. Je sens que quelque chose a maintenant changé.".

Part 8 - Temple sud-est

Le temple-se est au sud de la plaine-ee, au sud-est de la plaine-e, à l'est de la plaine-se et au nord-est de la plaine-ses. "[si le player est brumeux][brume][sinon]Un immense temple translucide évoquant la pureté même. Sa seule vision suffit à me faire frissonner tellement il est sublime. J'ai hâte d'entrer[fin si]."
Le printed name est "[si le player est brumeux][Brume][sinon]Devant un temple de cristal[fin si]".
Le temple-se est dans le nuage.

Chapter 1 - Intérieur du temple

Le temple-sei est à l'intérieur du temple-se. "Une unique et gigantesque pièce, avec au centre un piédestal accessible par un escalier.[si l' épée est sur le piédestal] Et au sommet…[saut de paragraphe]Une magnifique épée est scellée dans le cristal.[fin si][à la ligne]".
Le printed name est "À l'intérieur du temple".
Le temple-sei est dans le ciel.

Section 1 - Épée

L' épée (f) est une chose privately-named.
La description est "Une lame blanche, veinée de bleu, à la poignée d'argent. Elle ne semble pas avoir été touchée depuis des lustres.".
L' épée est sur le piédestal.

Comprendre "lame/epee" comme l' épée.
Understand "lame/epee celeste" as l' épée when l' épée est manipulée.

Instead of pulling l' épée, essayer de taking l' épée.

Instead of taking l' épée when le player n'est pas sur le piédestal, dire "Je dois d'abord monter sur le piédestal.".

Instead of taking l' épée when le Nâga n'est pas dans les mâchoires, dire "Je tire de toute mes forces, mais elle ne bouge pas d'un pouce.".

After taking l' épée:
	maintenant le player est brumeux;
	dire "Je tire de toute mes forces, mais l'épée glisse comme si elle sortait de l'eau. Je suis maintenant prêt à vaincre le Démon.";
	maintenant le printed name de l' épée est "Lame céleste";
	maintenant l' indefinite article de l' épée est "la";
	afficher la citation encadrée "Va aux ruines.".

Instead of dropping l' épée, dire "J'en ai besoin.".

Section 2 - Piédestal

Le piédestal est un support. Le piédestal est dans le temple-sei. Le piédestal est non décrit entrable privately-named.
La description est "Un magnifique ouvrage de cristal bleu[si l' épée est sur le piédestal] avec planté à l'intérieur une magnifique épée blanche[_]; l['][italique]Épée céleste[romain], à en croire la gravure sur le piédestal[fin si]."

Comprendre "piedestal" comme le piédestal.

Part 9 - Ruines

Chapter 1 - Extérieur des ruines

Les ruines sont au sud-ouest de la plaine-noo, à l'ouest de la plaine-oo et au nord-ouest de la plaine-soo. "[si le player est brumeux]Malgré le brouillard, je peux voir d[sinon]D[fin si]e majestueuses colonnes se dress[si le player est brumeux]ant[sinon]e[fin si] vers le ciel. Malheureusement, tout est en ruines ici, et je sens une aura maléfique nimber cet endroit.".
Le printed name est "Devant des ruines".
Les ruines sont dans le ciel.

After going to les ruines:
	afficher la citation encadrée "Le Démon de la Terre à réussi à s'introduire
	dans mon royaume en violant ce sanctuaire.
	Il faudra le repousser ici avant de pouvoir 
	le vaincre dans son domaine.
	Tout repose sur toi.";
	continuer l'action.

Section 1 - Tentacule

Le tentacule est à l'intérieur des ruines and à l'extérieur des ruines-intérieur. Le tentacule est une porte non ouvrable verrouillée. "Une sorte de gigantesque tentacule [si fermé]gesticule dans tous les sens, me barrant la route[sinon]gît à terre, coupé en deux[fin si]."
La description est "Un long tentacule pourvu d'excroissances rocheuses sort du sol. Elle doit être l'appendice de quelque chose de beaucoup plus gros se terrant dans les ruines.".

Instead of going dedans from les ruines when le tentacule est fermé:
	dire "Dans ma volonté de continuer, je m'approche de l'horreur qui me bloque le passage. Celle-ci n'a pas de pitié, et se saisit de moi.";
	attendre une touche;
	dire "Je n'ai pas le temps de réagir, l'étau se ressert et la créature me broie.";
	attendre une touche;
	fin de l'histoire en disant "Vous êtes mort[_]!".

Instead of opening le tentacule fermé:
	dire "Dans ma volonté de continuer, je m'approche de l'horreur qui me bloque le passage. Celle-ci n'a pas de pitié, et se saisit de moi.";
	attendre une touche;
	dire "Je n'ai pas le temps de réagir, son étau se ressert et la créature me broie.";
	attendre une touche;
	fin de l'histoire en disant "Vous êtes mort[_]!".

Instead of attacking le tentacule fermé:
	si le player porte l' épée:
		dire "Je lève la Lame bien haut avant de la laisser retomber violemment sur le tentacule. Il y a un flash aveuglant et l'immonde chose est coupée en deux, libérant le passage.";
		maintenant le tentacule est ouvert;
	sinon:
		dire "Je frappe le tentacule avec mes poings, espérant qu'il va se rétracter.";
		attendre une touche;
		dire "Pourtant, il s'allonge et me domine maintenant de toute sa hauteur. Je n'ai que le temps de le voir s'abattre sur moi";
		attendre une touche;
		fin de l'histoire en disant "Vous êtes mort[_]!".

Chapter 2 - Intérieur des ruines

Les ruines-intérieur sont dans le ciel. "Ici, il n'y a plus de cristal. Seule la roche grise et froide m'accueille. Cet endroit a été souillé par la corruption du Démon. Je n'ai pas envie de rester longtemps ici[si la fissure n'est pas une partie des murs-ruines]. Le plafond est effondré[fin si].".
Le printed name est "Entre des murs de pierre".
Les ruines-intérieurs sont privately-named.

Section 1 - Murs

Les murs-ruines sont des choses décoratives privately-named dans les ruines-intérieur.
La description est "Des murs froids de pierre, semblant complètement décalés dans ce monde de lumière. [si la fissure est une partie des murs-ruines]Une fissure inquiétante lézarde l'un d'eux[sinon]Le plafond est effondré, et un large flot de lumière pénètre en une large cascade[fin si].".

Comprendre "mur", "murs" ou "pierre" comme les murs-ruines.

Instead of attacking les murs-ruines when la fissure est une partie des murs-ruines, essayer de attacking la fissure.

Section 2 - Fissure

La fissure est une partie des murs-ruines.
La description est "Une large fissure sur l'un des murs, semblant mettre en péril le bâtiment entier.".

Instead of attacking la fissure pour la première fois:
	dire "De toutes mes forces, j'essaie d'élargir la fissure. Elle résiste, mais s'allonge tout de même jusqu'au plafond.";
	les murs s'effondrent in one turn from now.

Instead of opening la fissure, essayer de attacking la fissure.
Instead of attacking la fissure, dire "Elle est déjà bien assez grande comme ça.".

At the time when les murs s'effondrent:
	si le player est dans les ruines-intérieur:
		dire "Fragilisé par l'énorme fissure, le plafond s'écroule. Sur moi. C'est la fin.";
		attendre une touche;
		fin de l'histoire en disant "Vous êtes mort[_]!";
	retirer la fissure du jeu;
	maintenant l' oeil-ruines est ouvert.

Section 3 - Œil

L' oeil-ruines (m) est une chose privately-named dans les ruines-intérieur. "[si l' oeil-ruines est fermé]Un énorme œil semble incrusté dans un mur[sinon]La lumière semble avoir un effet négatif sur l[']œil[fin si].".
Le printed name est "œil".
La description de l' oeil-ruines est "[si fermé]Cet œil est résolument fermé et ne laisse voir que son épaisse paupière écailleuse[sinon]La lumière du ciel semble brûler l[']œil, et de la fumée s'en dégage[fin si]."
L' oeil-ruines peut être ouvert ou fermé. L' oeil-ruines est fermé.

Comprendre "oeil" comme l' oeil-ruines.

Instead of opening l' oeil-ruines fermé, dire "À peine effleuré-je la peau écailleuse qu'une vive brûlure se ressent. Je retire aussitôt ma main, faisant cesser la douleur.".

Instead of closing l' oeil-ruines ouvert, dire "Je devrais profiter de sa faiblesse pour frapper[_]!".

Instead of attacking l' oeil-ruines:
	si l' oeil-ruines est fermé:
		dire "Je brandis la Lame céleste et frappe l[']œil, mais celle-ci ripe sur les écailles. La chose ne semble pas avoir été dérangée.";
	sinon:
		dire "Profitant de la faiblesse de l[']œil, je parviens à planter l'épée dans le globe oculaire. Il finit par se désagréger et disparaître, révélant ainsi une sorte de socle.";
		retirer l' oeil-ruines du jeu;
		maintenant le socle est dans les ruines-intérieur;

Section 4 - Socle

Le socle est une chose décorative dans les ruines-intérieur.
La description est "Une plaque de pierre translucide, avec une forme en creux sculptée. À quoi peut-elle servir[_]?".

Instead of inserting quelque chose into le socle, dire "Ça ne rentre pas.".

Instead of inserting l' épée into le socle:
	dire "L'épée s'adapte parfaitement au socle, et elle commence à luire. Puis elle devient transparente, de plus en plus, avant de finalement disparaître. Où est-elle passée[_]?";
	maintenant le Nâga porte l' épée.

Instead of inserting la clef rouillée into le socle:
	dire "La clef s'adapte au socle, même si l'emplacement est bien trop grand. Elle commence à luire, devient transparente avant de finalement disparaître. Où est-elle passée[_]?";
	maintenant le Nâga porte la clef rouillée.

Part 10 - Chambre

La chambre est un endroit dans le ciel. "La tornade m'a porté dans un lieu étrange[_]: je me trouve maintenant dans une pièce lumineuse semblant être une chambre à coucher. Il y a ici un lit à baldaquin et surtout, un tableau au mur. Serais-je… dans la chambre de la Déesse du Ciel[_]? Dans tous les cas, une certaine nostalgie se dégage de l'endroit.[à la ligne]Il semblerait que je puisse quitter l'endroit par une sortie aveuglante.".
Le printed name est "Dans une chambre à coucher".

Instead of exiting from chambre, maintenant le player est dans l' emplacement de la tornade.

Chapter 1 - Lit

Le bed est une chose décorative privately-named dans la chambre.
Le printed name est "lit".
La description est "Le lit est parfaitement fait, on aurait même dit qu'il n'a jamais été occupé. Les tentures colorées de nombreuses teintes de bleu semblent extrêmement précieuses, de grande valeur. Où ai-je bien pu atterrir[_]?".

Comprendre "lit" comme le bed.

Chapter 2 - Tableau

Le tableau est une chose décorative dans la chambre.
La description est "Un œuvre de maître, portrait de deux enfants[_]:  une petite fille blonde et un garçon, plus grand et brun. Tous deux sourient et se tiennent par la main[_]; ils ont l'air heureux. Il y a une petite plaque fixée sous la toile indiquant [italique]En souvenir de la personne qui fut autrefois un frère, et non le Léviathan[romain].".

Carry out examining le tableau pour la première fois:
	incrémenter le score.

Volume 3 - Release
	
Le story headline est "Une fiction interactive".
Le story genre est "Fantasy".
Le release number est 1.
La story description est "Le Démon de la Terre s'est éveillé et, accomplissant une ancienne prophétie, vous vous êtes rendu sur une île déserte pour y chercher l'Orbe Étincelant, seule arme capable de le vaincre. Après avoir affronté de nombreux périls et trouvé l'artéfact divin, vous retournez maintenant parmi les vôtres, espérant qu'il n'est pas trop tard et que l'on pourra vous indiquer que faire à présent...
Ce jeu est la suite du ''Temple Nâga''.".
La story creation year est 2011.

Release along with cover art and the source text.

Book 1 - Commandes

Part 1 - Aide

After printing the banner text:
	dire "[première fois]Tapez «[_]aide[_]» pour obtenir plus d'informations sur le jeu ou si vous jouez à une fiction interactive pour la première fois.[à la ligne][unicode 8656] Avant de commencer, assurez-vous d'avoir joué au premier volet, [italique]le Temple Nâga[romain][_]![seulement]"

Understand "aide", "infos", "info", "informations" or "information" as a mistake ("Ce jeu est une fiction interactive[_]; il se joue en tapant des commandes à l'infinitif telles que «[_]voir[_]», «[_]prendre [italique]objet[romain][_]», etc. (pour plus d'informations, consultez [italique]www.ifiction.free.fr[romain]).[à la ligne]Tapez «[_]licence[_]» pour l'obtenir et «[_]auteur[_]» pour en savoir plus sur l'auteur.").

Part 2 - Licence

Understand "licence" or "license" as a mistake ("[gras]Pour faire bref[_]:[à la ligne][romain]Ce programme est un logiciel libre[_]; vous pouvez le redistribuer ou le modifier suivant les termes de la «[_]GNU General Public License[_]» telle que publiée par la Free Software Foundation[_]: soit la version 3 de cette licence, soit toute version ultérieure.[saut de paragraphe]Ce programme est distribué dans l'espoir qu'il vous sera utile, mais SANS AUCUNE GARANTIE[_]: sans même la garantie implicite de COMMERCIALISABILITÉ ni d'ADÉQUATION À UN OBJECTIF PARTICULIER. Consultez la Licence Générale Publique GNU pour plus de détails.[saut de paragraphe][gras]Pour faire long, [romain]consultez[_]: [italique]http://www.gnu.org/licenses[romain].").

Part 3 - Auteur

Understand "credits", "credit", "auteur" or "auteurs" as a mistake ("Il n'y a pas encore grand-chose à dire sur moi, si ce n'est que mon pseudonyme est Natrium. Ce jeu est l'une des premières fictions interactives que j'ai faites, il y aura donc probablement une autre version avec un lien vers un site ou autre. Patience… (mais vous pouvez quand même me contacter à cette adresse[_]: [italique]natrium729@gmail.com[romain])").

Part 4 - Test

Test lave with "s / parler à prêtresse / x orbe / écouter orbe / x stèle / lire stèle / frapper stèle / haut / parler / poser orbe sur autel / écouter orbe / sortir / bas / e / pr stalagmite / encore / crever oeil / o / se laver / écouter orbe / no / no / entrer / prier / écouter orbe / e / n".

Test crane with "pr lampe / ouvrir sac / pr cristal / insérer cristal dans lampe / n / pr pioche / attaquer gravats / o / pr crâne / écouter orbe / prier /pr canne /sortir / * tu dois trouver la tornade".

Test boussole with "écouter orbe / poser crâne / e / n / o / o / écouter orbe".

Test final with "sortir / est / est / nord-est / prier / écouter orbe / pr lampe / e / e / bas / écouter orbe / entrer / monter sur piédestal / pr épée / sortir / sortir / o /o / o / o / no / attaquer tentacule / entrer / ouvrir fissure / sortir / entrer / attaquer oeil / insérer lame dans socle / écouter orbe / attaquer dents / entrer".